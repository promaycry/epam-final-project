package com.epam.ua.dnipro.web.command;

import com.epam.ua.dnipro.kaliuha.web.command.CommandContainer;
import com.epam.ua.dnipro.kaliuha.web.command.ListExpositionsCommand;
import com.epam.ua.dnipro.kaliuha.web.command.NoCommand;
import org.junit.Assert;
import org.junit.Test;

public class CommandContainerTest {

    @Test
    public void testGet(){
        Assert.assertEquals(ListExpositionsCommand.class,CommandContainer.get("listOfExpositions").getClass());
    }

    @Test
    public void testGetNotExisting(){
        Assert.assertEquals(NoCommand.class,CommandContainer.get("nothing").getClass());
    }

}
