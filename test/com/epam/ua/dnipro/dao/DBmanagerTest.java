package com.epam.ua.dnipro.dao;

import com.epam.ua.dnipro.kaliuha.dao.DBManager;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;

import java.sql.Connection;
import java.sql.SQLException;

public class DBmanagerTest {

    @Mock
    Connection connection;

    @Before
    public void setUpConnection() throws SQLException {
       connection= DBManager.getInstance().getConnectionByDriverManager();
    }

    @Test
    public void testInstance() throws SQLException {
        DBManager dbManager=Mockito.mock(DBManager.class);
        Mockito.when(dbManager.getConnection()).thenReturn(
                (connection));
        Assert.assertNotNull(DBManager.getInstance());
        Assert.assertEquals(connection,dbManager.getConnection());
    }

    @Test
    public void testConnectionRollback() throws SQLException {
        DBManager dbManager=Mockito.mock(DBManager.class);
        Mockito.when(dbManager.getConnection()).thenReturn(
                (connection));
        Connection con=dbManager.getConnection();
        DBManager.rollbackAndClose(con);
        Assert.assertTrue(con.isClosed());
    }

    @Test
    public void testConnectionCommit() throws SQLException {
        DBManager dbManager=Mockito.mock(DBManager.class);
        Mockito.when(dbManager.getConnection()).thenReturn(
                (connection));
        Connection con=dbManager.getConnection();
        DBManager.commitAndClose(con);
        Assert.assertTrue(con.isClosed());
    }

    @Test
    public void testConnection() throws SQLException {
        Assert.assertNotNull(DBManager.getInstance().getConnectionByDriverManager());
    }

}
