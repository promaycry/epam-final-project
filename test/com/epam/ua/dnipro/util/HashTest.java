package com.epam.ua.dnipro.util;

import com.epam.ua.dnipro.kaliuha.util.Hash;
import org.junit.Assert;
import org.junit.Test;

public class HashTest {

    private static final String TEST="098F6BCD4621D373CADE4E832627B4F6";

    @Test
    public void testHash(){
        Assert.assertEquals(TEST, Hash.hash("test"));
    }

}
