package com.epam.ua.dnipro.util;

import com.epam.ua.dnipro.kaliuha.util.ExpositionsQuery;
import org.junit.Assert;
import org.junit.Test;

public class ExpositionsQueryTest {

    private static final String USER_DEFAULT="Select * from get_expositions_for_user where locale_id=? limit ?,?";

    private static final String ADMIN_DEFAULT="Select * from get_expositions where locale_id=? limit ?,?";

    private static final String USER_TOPIC_ASC="Select * from get_expositions_for_user where locale_id=? order by topic limit ?,?";

    private static final String ADMIN_TOPIC_ASC="Select * from get_expositions where locale_id=? order by topic limit ?,?";


    @Test
    public void testNull(){
        Assert.assertEquals(USER_DEFAULT, ExpositionsQuery.get("sds","topicAsc"));
        Assert.assertEquals(USER_DEFAULT, ExpositionsQuery.get("admin","dsds"));
        Assert.assertEquals(USER_DEFAULT, ExpositionsQuery.get("",""));
    }

    @Test
    public void testUser(){
        Assert.assertEquals(USER_DEFAULT, ExpositionsQuery.get("user","default"));
        Assert.assertEquals(USER_TOPIC_ASC, ExpositionsQuery.get("user","topicAsc"));
    }

    @Test
    public void testAdmin(){
        Assert.assertEquals(ADMIN_DEFAULT, ExpositionsQuery.get("admin","default"));
        Assert.assertEquals(ADMIN_TOPIC_ASC, ExpositionsQuery.get("admin","topicAsc"));
    }

}
