package com.epam.ua.dnipro.entity;

import com.epam.ua.dnipro.kaliuha.entity.Exposition;
import com.epam.ua.dnipro.kaliuha.entity.Hall;
import org.junit.Assert;
import org.junit.Test;

import java.math.BigDecimal;
import java.sql.Date;
import java.time.LocalDate;

public class ExpositionTest {

    @Test
    public void testExpositionNull(){
        Exposition exposition=new Exposition();
        Assert.assertEquals(0,exposition.getHalls().size());
        Assert.assertNull(exposition.getCost());
        Assert.assertNull(exposition.getDate());
        Assert.assertNull(exposition.getTicketsNum());
        Assert.assertNull(exposition.getImage());
        Assert.assertNull(exposition.getTopic());
        Assert.assertNull(exposition.getVisits());
        Assert.assertNull(exposition.getId());
    }

    @Test
    public void testExpositionSet(){
        Exposition exposition=new Exposition();
        Hall hall=new Hall();
        hall.setId((long)1);
        exposition.getHalls().add(hall);
        Assert.assertEquals(1,exposition.getHalls().size());
        exposition.setCost(new BigDecimal("30"));
        Assert.assertEquals(new BigDecimal("30"),exposition.getCost());
        exposition.setDate(Date.valueOf(LocalDate.now()));
        Assert.assertEquals(LocalDate.now().toString(),exposition.getDate().toString());
        exposition.setTicketsNum(10);
        Assert.assertEquals(Integer.valueOf(10),exposition.getTicketsNum());
        exposition.setTopic("test");
        Assert.assertEquals("test",exposition.getTopic());
        exposition.setVisits(3);
        Assert.assertEquals(Integer.valueOf(3),exposition.getVisits());
        exposition.setId((long) 1);
        Assert.assertEquals(Long.valueOf(1),exposition.getId());
        Assert.assertNotNull(exposition.toString());
    }

}
