package com.epam.ua.dnipro.entity;

import com.epam.ua.dnipro.kaliuha.entity.Hall;
import org.junit.Assert;
import org.junit.Test;

public class HallTest {

    @Test
    public void testHallNull(){
        Hall hall=new Hall();
        Assert.assertNull(hall.getId());
        hall.setId((long)3);
        Assert.assertNotNull(hall.getId());
    }

    @Test
    public void testHallConstructor(){
        Hall hall=new Hall((long)10);
        Assert.assertNotNull(hall.getId());
    }
}
