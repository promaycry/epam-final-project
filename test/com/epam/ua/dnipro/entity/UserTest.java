package com.epam.ua.dnipro.entity;

import com.epam.ua.dnipro.kaliuha.entity.User;
import org.junit.Assert;
import org.junit.Test;

import java.math.BigDecimal;

public class UserTest {

    @Test
    public void testUserNull(){
        User user=new User();
        Assert.assertNull(user.getPassword());
        Assert.assertNull(user.getRole());
        Assert.assertNull(user.getEmail());
        Assert.assertNull(user.getLogin());
        Assert.assertNull(user.getMoney());
        Assert.assertNull(user.getId());
    }

    @Test
    public void testUserSet(){
        User user=new User();
        user.setId((long)1);
        user.setMoney(new BigDecimal("30"));
        user.setPassword("password");
        user.setEmail("email");
        user.setRole("admin");
        user.setLogin("login");
        Assert.assertEquals(Long.valueOf((long)1),user.getId());
        Assert.assertEquals(new BigDecimal(30),user.getMoney());
        Assert.assertEquals("password",user.getPassword());
        Assert.assertEquals("email",user.getEmail());
        Assert.assertEquals("login",user.getLogin());
    }

}
