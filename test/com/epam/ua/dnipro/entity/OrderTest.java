package com.epam.ua.dnipro.entity;

import com.epam.ua.dnipro.kaliuha.entity.Exposition;
import com.epam.ua.dnipro.kaliuha.entity.Order;
import com.epam.ua.dnipro.kaliuha.entity.User;
import org.junit.Assert;
import org.junit.Test;

import java.math.BigDecimal;

public class OrderTest {

    @Test
    public void testNull(){
        Order order=new Order();
        Assert.assertNull(order.getExposition());
        Assert.assertNull(order.getUser());
        Assert.assertNull(order.getNumber());
        Assert.assertNull(order.getId());
        Assert.assertNull(order.getCost());
    }

    @Test
    public void testOrder(){
        Order order=new Order();
        Exposition exposition=new Exposition();
        order.setExposition(exposition);
        User user=new User();
        user.setId((long)1);
        order.setUser(user);
        order.setId((long)1);
        order.setCost(new BigDecimal("30"));
        order.setNumber(10);
        order.setDone(true);
        Assert.assertTrue(order.isDone());
        Assert.assertNotNull(order.getExposition());
        Assert.assertNotNull(order.getNumber());
        Assert.assertNotNull(order.getId());
        Assert.assertNotNull(order.getCost());
    }

}
