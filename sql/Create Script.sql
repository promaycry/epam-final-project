 
DROP schema if exists epam_final_project_data_base;

create schema epam_final_project_data_base;

use epam_final_project_data_base;

CREATE TABLE users_roles( 
id INT primary key,
role_name VARCHAR(15) not null,
constraint users_roles_id check(id>=1)
);

INSERT INTO users_roles(id,role_name) 
VALUES(1,'admin'),
(2,'authorized user');

CREATE TABLE users(
role_id int REFERENCES users_roles(id) ,
id int auto_increment primary key,
login varchar(20) unique NOT null,
password varchar(100) NOT NULL,
email varchar(40) not null,
money decimal(13,2) not null default 0,
constraint check_money check(money>=0),
constraint check_email check(email regexp '^[a-zA-Z0-9]+@[a-z]+\.(com||ua||ru||net||org)$')
);

INSERT INTO users(login,password,role_id,email) 
values('admin','21232F297A57A5A743894A0E4A801FC3',1,'promaycrynewtelefon@gmail.com');
INSERT INTO users(login,password,role_id,money,email) 
values('user','EE11CBB19052E40B07AAC0CA060C23EE',2,300,'user@gmail.org');

Create table halls(
id int primary key,
CONSTRAINT halls_id CHECK(ID>=1)
);

INSERT INTO halls(id) values(1),(2),(3),(4);

Create table expositions(
id int primary key auto_increment,
date date not null,
tickets_num int default 1,
visits int not null default 0 check(visits>=0),
cost decimal(13,2) default 0,
image longblob not null,
constraint check_cost check(cost>=0),
constraint check_visits check(visits<=tickets_num)
);

Create table exposition_halls(
exposition_id int ,
hall_id int,
foreign key(exposition_id) references expositions(id) On DELETE CASCADE,
foreign key(hall_id) references halls(id) ON DELETE CASCADE
);

create table locales(
id int primary key,
name varchar(5)
);

Create table exposition_names(
exposition_id int ,
topic varchar(100) not null unique,
locale_id int , 
foreign key(locale_id) references locales(id) ,
foreign key(exposition_id) references expositions(id) on delete cascade
); 

insert into locales(id,name) values(1,'en'),(2,'ru'),(3,'ua');

Create table orders(
id int auto_increment primary key,
exposition_id int references expositions(id),
cost decimal(13,2),
user_id int references users(id),
number_Of_Tickets int
);

Create view get_Expositions_for_user as 
Select e.id,e.tickets_num,e.date,e.visits,en.topic,en.locale_id,e.cost ,e.image
from expositions e 
join exposition_names en 
where en.exposition_id=e.id and e.date >=curdate() and e.visits<tickets_num;

Create view get_Expositions as 
Select e.id,e.tickets_num,e.date,e.visits,en.topic,en.locale_id,e.cost,e.image
from expositions e 
join exposition_names en 
where en.exposition_id=e.id and date>=curdate()
;

DELIMITER //
//

Create trigger update_expositions_delete after delete on orders
FOR EACH ROW BEGIN 
 update users set money=money+old.cost where id=old.user_id;
END;
//

create trigger insert_order before insert on orders
for each row begin
SET new.cost=NEW.number_Of_Tickets*(Select cost from expositions where id=new.exposition_id);
update users set money=money-NEW.cost where id=NEW.user_id;
update expositions set visits=visits+new.number_Of_Tickets where id =new.exposition_id ;
end;
//

create trigger update_order before update on orders
for each row begin
SET new.number_Of_Tickets=old.number_Of_Tickets+new.number_Of_Tickets;
SET new.cost=NEW.number_Of_Tickets*(Select cost from expositions where id=new.exposition_id);
update users set money=money-(NEW.cost-OLD.cost) where id=NEW.user_id;
update expositions set visits=visits+(NEW.number_Of_Tickets-OLD.number_Of_Tickets) where id=new.exposition_id;
end;
//

Create view get_Expositions_for_orders as 
Select e.id,e.tickets_num,e.date,e.visits,en.topic,en.locale_id,e.cost,e.image
from expositions e 
join exposition_names en 
where en.exposition_id=e.id;
//
