package com.epam.ua.dnipro.kaliuha.dao.mapper;

import com.epam.ua.dnipro.kaliuha.dao.Fields;
import com.epam.ua.dnipro.kaliuha.dao.HallDAO;
import com.epam.ua.dnipro.kaliuha.entity.Exposition;
import org.apache.log4j.Logger;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Extracts a exposition from the result set row.
 */
public class ExpositionMapper implements EntityMapper<Exposition> {

    private static final Logger log = Logger.getLogger(OrderMapper.class);

    @Override
    public Exposition mapRow(ResultSet rs) {
        try {
            Exposition exposition = new Exposition();
            exposition.setId(rs.getLong(Fields.ID));
            String topic=rs.getString(Fields.TOPIC);
            exposition.setTopic(topic.substring(0,topic.length()-3));
            exposition.setVisits(rs.getInt(Fields.VISITS));
            exposition.setCost(rs.getBigDecimal(Fields.COST));
            exposition.setTicketsNum(rs.getInt(Fields.TICKETS_NUM));
            exposition.setDate(rs.getDate(Fields.DATE));
            log.trace("date--->"+exposition.getDate());
            exposition.setImage(rs.getBlob(Fields.IMAGE));
            exposition.getHalls().addAll(new HallDAO().getExpositionsHalls(exposition));
            return exposition;
        } catch (SQLException e) {
            throw new IllegalStateException(e);
        }
    }

}
