package com.epam.ua.dnipro.kaliuha.dao.mapper;

import com.epam.ua.dnipro.kaliuha.dao.ExpositionDAO;
import com.epam.ua.dnipro.kaliuha.dao.Fields;
import com.epam.ua.dnipro.kaliuha.dao.UserDAO;
import com.epam.ua.dnipro.kaliuha.entity.Order;
import com.epam.ua.dnipro.kaliuha.web.Controller;
import org.apache.log4j.Logger;

import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;

/**
 * Extracts a order from the result set row.
 */
public class OrderMapper implements EntityMapper<Order>{


    private static final Logger log = Logger.getLogger(OrderMapper.class);

    @Override
    public Order mapRow(ResultSet rs) {
        try {
            Order order = new Order();
            order.setId(rs.getLong(Fields.ID));
            order.setCost(rs.getBigDecimal(Fields.COST));
            order.setUser(new UserDAO().getByID(rs.getLong(Fields.USER_ID)));
            order.setExposition(new ExpositionDAO().getByID(rs.getLong(Fields.EXPOSITION_ID),
                    rs.getInt(Fields.LOCALE_ID)));
            if(order.getExposition().getDate().compareTo(LocalDate.now())>0){
                order.setDone(false);
            }
            else{
                order.setDone(true);
            }
            order.setNumber(rs.getInt(Fields.NUMBER_OF_TICKETS));
            return order;
        } catch (SQLException e) {
            throw new IllegalStateException(e);
        }
    }

}
