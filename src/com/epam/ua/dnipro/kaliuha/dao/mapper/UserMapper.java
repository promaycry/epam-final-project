package com.epam.ua.dnipro.kaliuha.dao.mapper;

import com.epam.ua.dnipro.kaliuha.dao.Fields;
import com.epam.ua.dnipro.kaliuha.entity.User;
import com.epam.ua.dnipro.kaliuha.util.Hash;

import java.sql.ResultSet;
import java.sql.SQLException;

import static com.epam.ua.dnipro.kaliuha.dao.UserDAO.getUserRole;


/**
 * Extracts a user from the result set row.
 */
public class UserMapper implements EntityMapper<User> {

    @Override
    public User mapRow(ResultSet rs) {
        try {
            User user = new User();
            user.setId(rs.getLong(Fields.ID));
            user.setLogin(rs.getString(Fields.LOGIN));
            user.setRole(getUserRole(rs.getLong(Fields.ROLE_NAME_ID)));
            user.setPassword(rs.getString(Fields.PASSWORD));
            user.setEmail(rs.getString(Fields.EMAIL));
            user.setMoney(rs.getBigDecimal(Fields.MONEY));
            return user;
        } catch (SQLException e) {
            throw new IllegalStateException(e);
        }
    }

}
