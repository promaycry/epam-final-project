package com.epam.ua.dnipro.kaliuha.dao;


/**
 * Holder for fields names of DB tables.
 *
 * @author Kaliuha Aleksei
 *
 */
public final class Fields {

    public static final String TOPIC="topic";

    public static final String VISITS ="visits" ;

    public static final String IMAGE="image";

    public static final String TICKETS_NUM = "tickets_num";

    public static final String DATE ="date" ;

    public static final String ROLE_NAME ="role_name";

    public static final String LOGIN ="login";

    public static final String ID="id";

    public static final String NUMBER_OF_TICKETS="number_of_tickets";

    public static final String ROLE_NAME_ID ="role_id";

    public static final String PASSWORD ="password";

    public static final String HALL_ID="hall_id";

    public static final String COST="cost";

    public static final String EMAIL="email";

    public static final String NAME="name";

    public static final String USER_ID="user_id";

    public static final String EXPOSITION_ID="exposition_id";

    public static final String LOCALE_ID="locale_id";

    public static final String MONEY="money";

    private Fields(){}

}
