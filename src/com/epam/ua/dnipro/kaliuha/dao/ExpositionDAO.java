package com.epam.ua.dnipro.kaliuha.dao;

import com.epam.ua.dnipro.kaliuha.dao.mapper.ExpositionMapper;
import com.epam.ua.dnipro.kaliuha.entity.Exposition;
import com.epam.ua.dnipro.kaliuha.entity.Hall;

import java.io.InputStream;
import java.sql.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static com.epam.ua.dnipro.kaliuha.dao.DBManager.commitAndClose;
import static com.epam.ua.dnipro.kaliuha.dao.DBManager.rollbackAndClose;


/**
 * Data access object for exposition entity.
 */
public class ExpositionDAO{

    private static final String SQL_GET_EXPOSITION_BY_ID = "SELECT * FROM get_expositions_for_orders where id=? and locale_id=?";

    private static final String SQL_ADD_EXPOSITION = "Insert into expositions(date,tickets_num,cost,image) values(?,?,?,?)";

    private static final String SQL_SET_VISITS = "Update expositions set visits=visits-? where id=?";

    private static final String SQL_DELETE_EXPOSITION = "DELETE FROM expositions where id=?";

    private static final String SQL_GET_COUNT = "Select count( distinct id) from get_expositions_for_user";

    private static final String SQL_GET_COUNT_ADMIN = "Select count(distinct id) from get_expositions";

    private static final String SQL_GET_EXPOSITION_BY_NAME = "Select * from get_expositions where topic=?";
    /**
     * Get all expositions localized and sorted(or not),paginated
     *
     * @param locale which language will be used
     *
     * @param query which sql query will be used
     *
     * @param currentPage page for pagination
     *
     * @param perPage how much values per page
     *
     * @return list of expositions
     *
     */
    public List<Exposition> getAll(String locale, String query, int currentPage, int perPage) {
        int start = currentPage * perPage - perPage;
        List<Exposition> expositions = new ArrayList<>();
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        Connection con = null;
        ExpositionMapper expositionMapper = new ExpositionMapper();
        try {
            con = DBManager.getInstance().getConnection();
            pstmt = con.prepareStatement(query);
            pstmt.setInt(1, new LocaleDAO().getIdLocale(locale));
            pstmt.setInt(2, start);
            pstmt.setInt(3, perPage);
            rs = pstmt.executeQuery();
            while (rs.next()) {
                expositions.add(expositionMapper.mapRow(rs));
            }
            rs.close();
            pstmt.close();
        } catch (SQLException ex) {
            ex.printStackTrace();
            rollbackAndClose(con);
        } finally {
            commitAndClose(con);
        }
        return expositions;
    }

    /**
     * Get count of rows in all table
     *
     * @param b false-get for user,true-get for admin
     *
     * @return count of rows
     *
     */
    public int getCount(boolean b) {
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        Connection con = null;
        int count = 0;
        try {
            con = DBManager.getInstance().getConnection();
            pstmt = con.prepareStatement(SQL_GET_COUNT);
            if(b){
                pstmt=con.prepareStatement(SQL_GET_COUNT_ADMIN);
            }
            rs = pstmt.executeQuery();
            if (rs.next()) {
                count = rs.getInt(1);
            }
            rs.close();
            pstmt.close();
        } catch (SQLException ex) {
            ex.printStackTrace();
            rollbackAndClose(con);
        } finally {
            commitAndClose(con);
        }
        return count;
    }

    /**
     * Delete exposition
     *
     * @param exposition
     *
     *
     */
    public void delete(Exposition exposition) {
        Connection con = null;
        try {
            con = DBManager.getInstance().getConnection();
            PreparedStatement pstmt = con.prepareStatement(SQL_DELETE_EXPOSITION);
            pstmt.setLong(1, exposition.getId());
            pstmt.executeUpdate();
            pstmt.close();
            con.commit();
            new OrderDAO().deleteExp(exposition);
        } catch (SQLException ex) {
            DBManager.getInstance().rollbackAndClose(con);
            ex.printStackTrace();
        } finally {
            DBManager.getInstance().commitAndClose(con);
        }
    }


    /**
     * Update expositions visits by adding
     *
     * @param number
     *
     * @param id id of exposition
     */
    public void updateExposition(Integer number, Long id) {
        Connection con = null;
        try {
            con = DBManager.getInstance().getConnection();
            PreparedStatement pstmt = con.prepareStatement(SQL_SET_VISITS);
            pstmt.setInt(1, number);
            pstmt.setLong(2, id);
            pstmt.executeUpdate();
            pstmt.close();
            con.commit();
        } catch (SQLException ex) {
            DBManager.getInstance().rollbackAndClose(con);
            ex.printStackTrace();
        } finally {
            DBManager.getInstance().commitAndClose(con);
        }
    }
    /**
     * Get know about exist of expositions by topic
     *
     * @param topic
     *
     * @return true-if exist,false-if not
     */
    public boolean getByTopic(String topic) {
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        Connection con = null;
        try {
            con = DBManager.getInstance().getConnection();
            pstmt = con.prepareStatement(SQL_GET_EXPOSITION_BY_NAME);
            pstmt.setString(1, topic);
            rs = pstmt.executeQuery();
            if (rs.next())
                return true;
            rs.close();
            pstmt.close();
        } catch (SQLException ex) {
            rollbackAndClose(con);
            ex.printStackTrace();
        } finally {
            commitAndClose(con);
        }
        return false;
    }

    /**
     * Add expositions
     *
     * @param entity date,cost,number,image of tickets will be used
     *
     * @param topics locales and topics for localize
     *
     * @return true-if exist,false-if not
     */
    public void add(Exposition entity, HashMap<String, String> topics, InputStream inputStream) {
        Connection con = null;
        ResultSet resultSet = null;
        int id = 0;
        try {
            con = DBManager.getInstance().getConnection();
            PreparedStatement pstmt = con.prepareStatement(SQL_ADD_EXPOSITION, Statement.RETURN_GENERATED_KEYS);
            pstmt.setDate(1, Date.valueOf(entity.getDate()));
            pstmt.setInt(2, entity.getTicketsNum());
            pstmt.setBigDecimal(3, entity.getCost());
            pstmt.setBlob(4,inputStream);
            pstmt.executeUpdate();
            con.commit();
            resultSet = pstmt.getGeneratedKeys();
            if (resultSet.next()) {
                id = resultSet.getInt(1);
                for (String locale : LocaleDAO.getLocales()) {
                    new LocaleDAO().insertLocaleTopic(id, topics.get(locale), locale);
                }
                for (Hall hall : entity.getHalls()) {
                    new HallDAO().insertHall(id, hall.getId());
                }
            }
            pstmt.close();
        } catch (SQLException ex) {
            Exposition exposition = new Exposition();
            exposition.setId((long) id);
            delete(exposition);
            rollbackAndClose(con);
            ex.printStackTrace();
        } finally {
            DBManager.commitAndClose(con);
        }
    }


    /**
     * Get exposition by id and locale
     *
     *
     * @param id id of exposition
     *
     * @param locale id of locale
     *
     * @return Exposition or null if not exist
     */
    public Exposition getByID(Long id, int locale) {
        Exposition exposition = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        Connection con = null;
        ExpositionMapper expositionMapper = new ExpositionMapper();
        try {
            con = DBManager.getInstance().getConnection();
            pstmt = con.prepareStatement(SQL_GET_EXPOSITION_BY_ID);
            pstmt.setLong(1, id);
            pstmt.setInt(2, locale);
            rs = pstmt.executeQuery();
            if (rs.next()) {
                exposition = expositionMapper.mapRow(rs);
            }
            rs.close();
            pstmt.close();
        } catch (SQLException ex) {
            ex.printStackTrace();
            rollbackAndClose(con);
        } finally {
            commitAndClose(con);
        }
        return exposition;
    }
}
