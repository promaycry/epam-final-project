package com.epam.ua.dnipro.kaliuha.web.filter;

import com.epam.ua.dnipro.kaliuha.entity.User;
import com.epam.ua.dnipro.kaliuha.web.Path;
import org.apache.log4j.Logger;

import javax.servlet.*;
import javax.servlet.http.HttpFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.*;

/**
 * Security filter.
 *
 * @author Kaliuha Aleksei
 *
 */

public class AccessFilter extends HttpFilter {

    private static final Logger log = Logger.getLogger(AccessFilter.class);
    // commands access
    private static Map<String, List<String>> accessMap = new HashMap<>();
    private static List<String> outOfControl = new ArrayList<String>();


    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
            throws IOException, ServletException {
        log.debug("Filter starts");
        HttpServletRequest httpRequest = (HttpServletRequest)request;
        log.trace("Request uri --> " + httpRequest.getRequestURI());
        if (accessAllowed(request)) {
            chain.doFilter(request, response);
        } else {
            String errorMessasge = "permissionFailed";

            request.setAttribute("errorMessage", errorMessasge);

            log.trace("Set the request attribute: errorMessage --> " + errorMessasge);

            request.getRequestDispatcher(Path.PAGE_ERROR_PAGE)
                    .forward(request, response);
        }
    }

    private boolean accessAllowed(ServletRequest request) {
        HttpServletRequest httpRequest = (HttpServletRequest) request;

        String commandName = request.getParameter("command");
        if (commandName == null || commandName.isEmpty())
            return false;

        if (outOfControl.contains(commandName))
            return true;

        HttpSession session = httpRequest.getSession(false);
        if (session == null)
            return false;

        User user = (User)session.getAttribute("user");
        String role=null;
        if(user==null){
            role="unAuthorized user";
        }
        else {
            role=user.getRole();
        }

        return accessMap.get(role).contains(commandName)||outOfControl.contains(commandName);
    }

    public void init(FilterConfig fConfig) throws ServletException {
        log.debug("Filter initialization starts");
        // roles
        accessMap.put("admin", asList(fConfig.getInitParameter("admin")));
        accessMap.put("authorized user", asList(fConfig.getInitParameter("authorized user")));
        accessMap.put("unAuthorized user",asList(fConfig.getInitParameter("unAuthorized user")));
        log.trace("Access map --> " + accessMap);

        // out of control
        outOfControl = asList(fConfig.getInitParameter("common"));
        log.trace("Out of control commands --> " + outOfControl);
        log.debug("Filter initialization finished");

    }

    @Override
    public void destroy() {
        log.debug("Filter destruction starts");
        // do nothing
        log.debug("Filter destruction finished");
    }

    /**
     * Extracts parameter values from string.
     *
     * @param str
     *            parameter values string.
     * @return list of parameter values.
     */
    private List<String> asList(String str) {
        List<String> list = new ArrayList<String>();
        StringTokenizer st = new StringTokenizer(str);
        while (st.hasMoreTokens()) list.add(st.nextToken());
        return list;
    }

}
