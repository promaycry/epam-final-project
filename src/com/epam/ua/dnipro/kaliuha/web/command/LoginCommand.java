package com.epam.ua.dnipro.kaliuha.web.command;

import com.epam.ua.dnipro.kaliuha.dao.UserDAO;
import com.epam.ua.dnipro.kaliuha.entity.User;
import com.epam.ua.dnipro.kaliuha.util.Hash;
import com.epam.ua.dnipro.kaliuha.web.Path;
import org.apache.log4j.Logger;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;


/**
 * Login command.
 *
 * @author Kaliuha Aleksei
 *
 */
public class LoginCommand extends Command {

    private static final long serialVersionUID = -3071536593627692473L;

    private static final Logger log = Logger.getLogger(LoginCommand.class);

    public   static  final List<User>loginedUsers= Collections.synchronizedList(new ArrayList<>());

    public static synchronized boolean add(User user){
        if(loginedUsers.contains(user)){
            return false;
        }
        loginedUsers.add(user);
        return true;
    }

    public static final synchronized void delete(User user){
        if(loginedUsers.contains(user)){
            loginedUsers.remove(user);
        }
    }

    @Override
    public void execute(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        log.debug("Command starts");

        HttpSession session = request.getSession();
        RequestDispatcher requestDispatcher=request.getRequestDispatcher(Path.PAGE_ERROR_PAGE);

        // obtain login and password from the request
        String login = request.getParameter("login");

        log.trace("Request parameter: login --> " + login);

        String password = Hash.hash(request.getParameter("password"));

        log.trace("Request parameter: password --> " + password);

        // error handler
        String errorMessage = null;

        if (login.isEmpty() || password.isEmpty()) {
            errorMessage = "loginPassword.empty";
            request.setAttribute("errorMessage", errorMessage);
            requestDispatcher.forward(request,response);
            log.error("errorMessage --> " + errorMessage);
            return;
        }

        User user = new UserDAO().get(login,password);
        log.trace("Found in DB: user --> " + user);

        if (user == null ) {
            errorMessage = "user.empty";
            request.setAttribute("errorMessage", errorMessage);
            requestDispatcher.forward(request,response);
            log.error("errorMessage --> " + errorMessage);
            return;
        }
        else {
            if(add(user)){
            session.setAttribute("user", user);
            log.trace("Set the session attribute: user --> " + user);
            }
            else{
                errorMessage = "user.logined";
                request.setAttribute("errorMessage", errorMessage);
                requestDispatcher.forward(request,response);
                log.error("errorMessage --> " + errorMessage);
                return;
            }
        }

        requestDispatcher=request.getRequestDispatcher(Path.MAIN);
        requestDispatcher.forward(request,response);
        log.debug("Command finished");
    }
}
