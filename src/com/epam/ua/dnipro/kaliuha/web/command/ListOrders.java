package com.epam.ua.dnipro.kaliuha.web.command;

import com.epam.ua.dnipro.kaliuha.dao.ExpositionDAO;
import com.epam.ua.dnipro.kaliuha.dao.LocaleDAO;
import com.epam.ua.dnipro.kaliuha.dao.OrderDAO;
import com.epam.ua.dnipro.kaliuha.dao.UserDAO;
import com.epam.ua.dnipro.kaliuha.entity.Order;
import com.epam.ua.dnipro.kaliuha.entity.User;
import com.epam.ua.dnipro.kaliuha.util.OrderQuery;
import com.epam.ua.dnipro.kaliuha.web.Path;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.jsp.jstl.core.Config;
import java.io.IOException;
import java.util.List;

public class ListOrders  extends Command{

    private static final Integer ELEMENTS =6;

    private static final long serialVersionUID = 1863978254689589999L;

    private static final Logger log = Logger.getLogger(ListExpositionsCommand.class);

    @Override
    public void execute(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        log.debug("Commands starts");

        HttpSession session=request.getSession();

        Cookie[]cookies=request.getCookies();

        boolean find=false;

        String locale=null;

        List<String>locales= LocaleDAO.getLocales();

        for(Cookie cookie:cookies){
            log.debug("Seeking coockies");
            if("defaultLocale".equals(cookie.getName())){
                log.debug("Coockies finded");
                locale=cookie.getValue();
                log.trace("locale set--->"+locale);
                Config.set(session,"javax.servlet.jsp.jstl.fmt.locale",locale);
                log.trace("locale of jsp`s set--->"+locale);
                session.setAttribute("defaultLocale",locale);
                log.trace("session atribute defaultLocale set--->"+locale);
                session.setAttribute("locales",locales);
                log.trace("session locales set--->"+locales);
                find=true;
                break;
            }
        }

        if(!find){
            log.debug("Create new coockie");
            locale= (String) session.getAttribute("javax.servlet.jsp.jstl.fmt.locale");
            log.trace("locale set-->"+locale);
            session.setAttribute("defaultLocale",locale);
            log.trace("session atribute defaultLocale set--->"+locale);
            session.setAttribute("locales",locales);
            log.trace("session locales set--->"+locales);
            Cookie cookie=new Cookie("defaultLocale",locale);
            response.addCookie(cookie);
            log.trace("coockie set--->"+cookie);
        }

        User user=(User)session.getAttribute("user");
        user=new UserDAO().getByID(user.getId());

        int page=Integer.valueOf(request.getParameter("currentPage"));

        String query= OrderQuery.get(request.getParameter("orderBy"));

        log.trace("sort by--->"+request.getParameter("orderBy"));

        log.trace("sql query--->"+query);

        List<Order> orderList=new OrderDAO().getUserOrders(user,locale,page,ELEMENTS,query);

        request.setAttribute("orderList",orderList);

        log.trace("orderList-->"+orderList);

        request.setAttribute("currentPage",page);
        log.trace("currentPage set --->"+page);

        int count=new OrderDAO().getCount(user);
        log.trace("count of orders --->"+count);


        int numberOfPages=count/ ELEMENTS;
        log.trace("number of pages--->"+numberOfPages);

        if(count% ELEMENTS !=0){
            numberOfPages++;
            log.trace("number of pages--->"+numberOfPages);
        }

        request.setAttribute("noOfPages",numberOfPages);
        log.trace("number of pages sended--->"+numberOfPages);

        request.setAttribute("currentView","orders");
        log.trace("set currentView-->orders");

        request.setAttribute("orderBy",request.getParameter("orderBy"));
        log.trace("set orderBy->"+request.getParameter("orderBy"));

        request.getRequestDispatcher(Path.PAGE_BUCKET).forward(request,response);

    }
}
