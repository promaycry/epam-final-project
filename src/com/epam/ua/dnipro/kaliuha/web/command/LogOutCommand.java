package com.epam.ua.dnipro.kaliuha.web.command;

import com.epam.ua.dnipro.kaliuha.entity.User;
import com.epam.ua.dnipro.kaliuha.web.Path;
import org.apache.log4j.Logger;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;


/**
 * Logout command.
 *
 * @author Kaliuha Aleksei
 *
 */
public class LogOutCommand extends Command {

    private static final long serialVersionUID = -2785976616686657267L;

    private static final Logger log = Logger.getLogger(LogOutCommand.class);

    @Override
    public void execute(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        log.debug("Command starts");
        HttpSession session = request.getSession(false);

        if (session != null){
            User user=(User)session.getAttribute("user");
            LoginCommand.delete(user);
            session.removeAttribute("user");
            log.trace("Session user in deleted");
        }

        RequestDispatcher requestDispatcher=request.getRequestDispatcher(Path.MAIN);
        requestDispatcher.forward(request,response);
        log.debug("Command finished");
    }
}
