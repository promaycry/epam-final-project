package com.epam.ua.dnipro.kaliuha.web.command.page;

import com.epam.ua.dnipro.kaliuha.dao.HallDAO;
import com.epam.ua.dnipro.kaliuha.dao.LocaleDAO;
import com.epam.ua.dnipro.kaliuha.entity.Hall;
import com.epam.ua.dnipro.kaliuha.web.Path;
import com.epam.ua.dnipro.kaliuha.web.command.Command;
import org.apache.log4j.Logger;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.jsp.jstl.core.Config;
import java.io.IOException;
import java.util.List;



/**
 * Go to page addExposition.jsp command.
 *
 * @author Kaliuha Aleksei
 *
 */
public class GoToPageAddCommand extends Command {

    private static final long serialVersionUID = 4748791314038076456L;

    private static final Logger log = Logger.getLogger(GoToPageAddCommand.class);
    @Override
    public void execute(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        log.debug("Command starts");
        List<Hall> hallList=new HallDAO().getHalls();
        log.trace("get halls--->"+hallList);
        request.setAttribute("Halls",hallList);
        log.trace("set halls--->"+hallList);
        request.setAttribute("currentView","addPage");
        log.trace("set currentView--->addPage");
        RequestDispatcher requestDispatcher=request.getRequestDispatcher(Path.PAGE_ADD);
        requestDispatcher.forward(request,response);
        log.debug("Command finished");
    }
}
