package com.epam.ua.dnipro.kaliuha.web.command;

import com.epam.ua.dnipro.kaliuha.dao.ExpositionDAO;
import com.epam.ua.dnipro.kaliuha.entity.Exposition;
import com.epam.ua.dnipro.kaliuha.web.Path;
import org.apache.log4j.Logger;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;


/**
 * Delete exposition command.
 *
 * @author Kaliuha Aleksei
 *
 */
public class DeleteExpositionCommand  extends Command{

    private static final long serialVersionUID = 7738791314029478343L;

    private static final Logger log = Logger.getLogger(DeleteExpositionCommand.class);

    @Override
    public void execute(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        log.debug("Command buyExposition starts");
        RequestDispatcher requestDispatcher=request.getRequestDispatcher(Path.PAGE_ERROR_PAGE);
        Exposition exposition=new Exposition();
        String id=request.getParameter("exposition_id");
        log.trace("exposition id-->"+id);
        if(id.isEmpty()){
            String error="expositionNumberToDelete";
            request.setAttribute("errorMessage",error);
            log.error("errorMessage --> " + error);
            requestDispatcher.forward(request,response);
            return;
        }
        exposition.setId(Long.valueOf(id));
        new ExpositionDAO().delete(exposition);
        log.trace("exposition deleted -->"+exposition.getId());
        String sort=request.getParameter("sortBy");
        Integer currentPage=Integer.valueOf(request.getParameter("currentPage"));
        response.sendRedirect("controller?command=listOfExpositions&sortBy="+sort+"&currentPage="+currentPage);
        log.debug("Command finished");
    }

}
