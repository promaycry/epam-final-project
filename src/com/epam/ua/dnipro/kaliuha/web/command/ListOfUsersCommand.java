package com.epam.ua.dnipro.kaliuha.web.command;

import com.epam.ua.dnipro.kaliuha.dao.UserDAO;
import com.epam.ua.dnipro.kaliuha.entity.User;
import com.epam.ua.dnipro.kaliuha.util.UserQuery;
import com.epam.ua.dnipro.kaliuha.web.Path;
import org.apache.log4j.Logger;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;


/**
 * Get all users
 *
 * @author Kaliuha Aleksei
 *
 */
public class ListOfUsersCommand  extends Command{

    private static final int PER_PAGE=6;

    private static final long serialVersionUID = 1863978254689581111L;

    private static final Logger log = Logger.getLogger(ListOfUsersCommand.class);

    @Override
    public void execute(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {

        log.debug("Command starts");
        UserDAO userDAO=new UserDAO();

        int count=userDAO.getCount();
        log.trace("count of user-->"+count);
        int numberOfPages=count/ PER_PAGE;

        if(count% PER_PAGE !=0){
            numberOfPages++;
        }
        log.trace("number of pages-->"+numberOfPages);
        int currentPage=Integer.valueOf(request.getParameter("currentPage"));
        log.trace("current page-->"+currentPage);
        String orderBy=request.getParameter("orderBy");
        log.trace("order by-->"+orderBy);
        String query= UserQuery.get(orderBy);

        log.trace("query-->"+query);

        List<User> users=userDAO.getUsers(query,currentPage,PER_PAGE);
        log.trace("get users-->"+users);

        request.setAttribute("noOfPages",numberOfPages);
        log.trace("set noOfPages-->"+numberOfPages);

        request.setAttribute("currentPage",currentPage);
        log.trace("set currentPage-->"+currentPage);

        request.setAttribute("orderBy",orderBy);
        log.trace("set orderBy-->"+orderBy);

        request.setAttribute("users",users);
        log.trace("set user-->"+users);

        request.setAttribute("currentView","users");
        log.trace("set currentView-->users");

        RequestDispatcher requestDispatcher=request.getRequestDispatcher(Path.LIST_OF_USERS);
        requestDispatcher.forward(request,response);

        log.debug("Command finished");
    }

}
