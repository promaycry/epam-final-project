package com.epam.ua.dnipro.kaliuha.web.command;

import com.epam.ua.dnipro.kaliuha.web.command.page.*;
import org.apache.log4j.Logger;

import java.util.Map;
import java.util.TreeMap;


/**
 * Holder for all commands.<br/>
 *
 * @author Kaliuha Aleksei
 *
 */
public class CommandContainer {

    private static final Logger log = Logger.getLogger(CommandContainer.class);

    private static Map<String, Command> commands = new TreeMap<>();

    static {
        log.debug("Command container initialize start");
        commands.put("listOfExpositions",new ListExpositionsCommand());
        commands.put("login",new LoginCommand());
        commands.put("buyExposition",new BuyExpositionsCommand());
        commands.put("noCommand",new NoCommand());
        commands.put("logOut",new LogOutCommand());
        commands.put("deleteOrder",new DeleteOrderCommand());
        commands.put("addExposition",new AddExpositionCommand());
        commands.put("goToPageBucket",new GoToBucketPageCommand());
        commands.put("listOfOrders",new ListOrders());
        commands.put("goToPageAdd",new GoToPageAddCommand());
        commands.put("goToPageList",new GoToPageList());
        commands.put("goToPageRegister",new GoToPageRegisterCommand());
        commands.put("setting",new SettingCommand());
        commands.put("signUp",new RegisterCommand());
        commands.put("updateMoney",new AddMoneyCommand());
        commands.put("deleteExposition",new DeleteExpositionCommand());
        commands.put("listOfUsers",new ListOfUsersCommand());
        commands.put("goToPageUsers",new GoToPageUsers());
        commands.put("updateUser",new UpdateUserCommand());
        log.debug("Command container was successfully initialized");
        log.trace("Number of commands --> " + commands.size());
    }



    /**
     * Returns command object with the given name.
     *
     * @param commandName
     *            Name of the command.
     * @return Command object.
     */

    public static Command get(String commandName) {
        System.out.println(commandName);
        if (commandName == null || !commands.containsKey(commandName)) {
            return commands.get("noCommand");
        }
        return commands.get(commandName);
    }
}
