package com.epam.ua.dnipro.kaliuha.web.command;

import com.epam.ua.dnipro.kaliuha.dao.ExpositionDAO;
import com.epam.ua.dnipro.kaliuha.dao.LocaleDAO;
import com.epam.ua.dnipro.kaliuha.entity.Exposition;
import com.epam.ua.dnipro.kaliuha.entity.Hall;
import com.epam.ua.dnipro.kaliuha.web.Path;
import org.apache.log4j.Logger;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.sql.Date;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;


/**
 * Add exposition command.
 *
 * @author Kaliuha Aleksei
 *
 */
public class AddExpositionCommand extends Command {

    private static final long serialVersionUID = 7738791314029476456L;

    private static final Logger log = Logger.getLogger(AddExpositionCommand.class);

    @Override
    public void execute(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        log.debug("Command addExposition starts");
        Exposition exposition=new Exposition();

        HashMap<String,String> names=new HashMap<>();

        String tickets=request.getParameter("tickets");

        request.setAttribute("currentView","addPage");

        if(tickets.isEmpty()){
            String error="numberInput";
            request.setAttribute("errorMessage",error);
            RequestDispatcher requestDispatcher=request.getRequestDispatcher(Path.PAGE_ERROR_PAGE);
            requestDispatcher.forward(request,response);
            log.error("error--->"+error);
            return;
        }

        exposition.setTicketsNum(Integer.valueOf(tickets));

        String price =request.getParameter("price");

        if(price.isEmpty()||!price.matches("[1-9][0-9]{0,}[.]{0,1}[0-9]{0,2}")){
            String error="priceInput";
            request.setAttribute("errorMessage",error);
            RequestDispatcher requestDispatcher=request.getRequestDispatcher(Path.PAGE_ERROR_PAGE);
            requestDispatcher.forward(request,response);
            log.error("error--->"+error);
            return;
        }

        exposition.setCost(new BigDecimal(price));

        log.trace("price--->"+price);

        String date=request.getParameter("date");

        if(date.isEmpty() ||date.compareTo(String.valueOf(LocalDate.now()))<0){
            String error="dateLess";
            request.setAttribute("errorMessage",error);
            RequestDispatcher requestDispatcher=request.getRequestDispatcher(Path.PAGE_ERROR_PAGE);
            requestDispatcher.forward(request,response);
            log.error("error--->"+error);
            return;
        }

        exposition.setDate(Date.valueOf(date));

        log.trace("date--->"+date);

        List<String> locales=LocaleDAO.getLocales();
        for (String locale: locales) {
            String parameter=request.getParameter("name_"+locale);
            log.trace("parameter--->"+parameter);
            System.out.println(parameter);
            if(new ExpositionDAO().getByTopic(new String(parameter.getBytes("ISO-8859-1"),"utf-8")
                    +"_"+locale)){
                String error="expositionExist";
                request.setAttribute("errorMessage",error);
                RequestDispatcher requestDispatcher=request.getRequestDispatcher(Path.PAGE_ERROR_PAGE);
                requestDispatcher.forward(request,response);
                log.error("error--->"+error);
                return;
            }
            else {
                log.trace("names put--->"+locale+","+new String(parameter.getBytes("ISO-8859-1"),
                        "utf-8")
                        +"_"+locale);
                names.put(locale,new String(parameter.getBytes("ISO-8859-1"),"utf-8")
                        +"_"+locale);
            }
        }

        log.trace("names count --->"+names.size());
        log.trace("locales--->"+locales.size());
        if(names.size()!=locales.size()){
            String error="allLocalesRequired";
            request.setAttribute("errorMessage",error);
            RequestDispatcher requestDispatcher=request.getRequestDispatcher(Path.PAGE_ERROR_PAGE);
            requestDispatcher.forward(request,response);
            log.error("error--->"+error);
            return;
        }

        log.trace("locale Topics --->"+names.toString());

        String[]halls=request.getParameterValues("halls");

        if(halls.length==0){
            String error="SelectHalls";
            request.setAttribute("errorMessage",error);
            RequestDispatcher requestDispatcher=request.getRequestDispatcher(Path.PAGE_ERROR_PAGE);
            requestDispatcher.forward(request,response);
            log.error("error--->"+error);
            return;
        }

        log.trace("halls-->"+ Arrays.toString(halls));

        List<Hall>hallTransofm=new ArrayList<>();
        for(String hall:halls){
            if(!hall.matches("[1-9]{1,}")){
                String error="hallNumber";
                request.setAttribute("errorMessage",error);
                RequestDispatcher requestDispatcher=request.getRequestDispatcher(Path.PAGE_ERROR_PAGE);
                requestDispatcher.forward(request,response);
                log.error("error--->"+error);
                return;
            }
            hallTransofm.add(new Hall(Long.valueOf(hall)));
            log.trace("hall --> "+hall);
        }

        InputStream inputStream = null; // input stream of the upload file

        // obtains the upload file part in this multipart request
        Part filePart = request.getPart("image");
        if (filePart != null) {
            inputStream = filePart.getInputStream();
        }


        exposition.getHalls().addAll(hallTransofm);
        log.trace("exposition halls--->"+exposition.getHalls());

        new ExpositionDAO().add(exposition,names,inputStream);
        log.trace("exposition add--->"+names.toString());

        response.sendRedirect(Path.GO_TO_PAGE_ADD);
        log.debug("Command addExposition finished");
    }
}
