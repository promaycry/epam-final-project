package com.epam.ua.dnipro.kaliuha.web.command;

import com.epam.ua.dnipro.kaliuha.dao.UserDAO;
import com.epam.ua.dnipro.kaliuha.entity.User;
import com.epam.ua.dnipro.kaliuha.web.Path;
import org.apache.log4j.Logger;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.math.BigDecimal;


/**
 * Add money command
 *
 * @author Kaliuha Aleksei
 *
 */
public class AddMoneyCommand  extends Command{

    private static final long serialVersionUID = 7738791314029478505L;

    private static final Logger log = Logger.getLogger(AddMoneyCommand.class);

    @Override
    public void execute(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {

        log.debug("Command buyExposition starts");

        String money=request.getParameter("money");
        RequestDispatcher requestDispatcher=request.getRequestDispatcher(Path.PAGE_ERROR_PAGE);
        log.trace("money-->"+money);

        if(money.isEmpty()||!money.matches("[1-9][0-9]{0,}[.]{0,1}[0-9]{0,2}")){
            String error="sumInvalid";
            request.setAttribute("errorMessage",error);
            requestDispatcher.forward(request,response);
            log.error("errorMessage --> " + error);
            return;
        }

        String numbercard= request.getParameter("cardNumber");
        log.trace("card number-->"+numbercard);

        if(numbercard.isEmpty()||!numbercard.matches("[0-9]{16}")){
            String error="cardNumberInvalid";
            request.setAttribute("errorMessage",error);
            requestDispatcher.forward(request,response);
            log.error("errorMessage --> " + error);
            return;
        }

        String cvv=request.getParameter("CVV");
        log.trace("CVV-->"+cvv);

        if(cvv.isEmpty()||!cvv.matches("[0-9]{3}")){
            String error="cardCVVInvalid";
            request.setAttribute("errorMessage",error);
            requestDispatcher.forward(request,response);
            log.error("errorMessage --> " + error);
            return;
        }

        String userName=request.getParameter("userName");
        log.trace("User name-->"+userName);
        if(userName.isEmpty()){
            String error="cardNameInvalid";
            request.setAttribute("errorMessage",error);
            requestDispatcher.forward(request,response);
            log.error("errorMessage --> " + error);
            return;
        }

        String mm=request.getParameter("MM");
        log.trace("Month of dissapear-->"+mm);
        String yy= request.getParameter("YY");
        log.trace("Year of dissapear-->"+yy);

        if(mm.isEmpty()||yy.isEmpty()){
            String error="cardMMYYInvalid";
            request.setAttribute("errorMessage",error);
            requestDispatcher.forward(request,response);
            log.error("errorMessage --> " + error);
            return;
        }

        HttpSession httpSession=request.getSession();
        User user=(User)httpSession.getAttribute("user");
        log.trace("Session user get-->"+user);

        new UserDAO().updateUserMoney(user,new BigDecimal(money));
        log.debug("User money updated");

        BigDecimal addMoney=new BigDecimal(money);
        addMoney.add(user.getMoney());

        log.trace("Session user set-->"+user);
        httpSession.setAttribute("user",user);

        String currentView=request.getParameter("currentView");
        log.trace("currrent view-->"+currentView);

        if(currentView.equals("expositions")){
            String sortBy=request.getParameter("sortBy");
            String currentPage=request.getParameter("currentPage");
            log.debug("redirect to expositions page");
            response.sendRedirect("controller?command=listOfExpositions&currentPage="+currentPage+"&sortBy="+sortBy);
        }

        if(currentView.equals("orders")){
            String curPage=request.getParameter("currentPage");
            String orderBy=request.getParameter("orderBy");
            log.debug("redirect to orders page");
            response.sendRedirect("controller?command=listOfOrders&currentPage="+curPage+"&orderBy="+orderBy);
        }

        log.debug("Command buyExposition finished");
    }

}
