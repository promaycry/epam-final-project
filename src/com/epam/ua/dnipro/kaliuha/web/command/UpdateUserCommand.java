package com.epam.ua.dnipro.kaliuha.web.command;

import com.epam.ua.dnipro.kaliuha.dao.UserDAO;
import com.epam.ua.dnipro.kaliuha.entity.User;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;


/**
 * Promote user to admin
 *
 * @author Kaliuha Aleksei
 *
 */
public class UpdateUserCommand extends Command {

    private static final long serialVersionUID = -1101976618667657267L;

    private static final Logger log = Logger.getLogger(UpdateUserCommand.class);

    @Override
    public void execute(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        log.debug("Command start");

        long id= Long.parseLong(request.getParameter("user"));
        log.trace("user id--->"+id);

        User user=new User();

        user.setId(id);

        new UserDAO().updateUserRole(user);
        log.debug("user with id+"+user.getId()+" updated");


        String orderBy=request.getParameter("orderBy");
        log.trace("order by--->"+orderBy);


        String currentPage=request.getParameter("currentPage");
        log.trace("current page--->"+currentPage);

        response.sendRedirect("controller?command=listOfUsers&orderBy="+orderBy+"&currentPage="+currentPage);
        log.debug("Command finished");
    }
}
