package com.epam.ua.dnipro.kaliuha.web.command;

import com.epam.ua.dnipro.kaliuha.dao.ExpositionDAO;
import com.epam.ua.dnipro.kaliuha.dao.LocaleDAO;
import com.epam.ua.dnipro.kaliuha.dao.OrderDAO;
import com.epam.ua.dnipro.kaliuha.dao.UserDAO;
import com.epam.ua.dnipro.kaliuha.entity.Exposition;
import com.epam.ua.dnipro.kaliuha.entity.User;
import com.epam.ua.dnipro.kaliuha.web.Path;
import org.apache.log4j.Logger;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.math.BigDecimal;


/**
 * Make an order command.
 *
 * @author Kaliuha Aleksei
 *
 */
public class BuyExpositionsCommand extends Command{

    private static final long serialVersionUID = 7732286214029478505L;

    private static final Logger log = Logger.getLogger(BuyExpositionsCommand.class);

    @Override
    public void execute(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        log.debug("Command buyExposition starts");
        RequestDispatcher requestDispatcher=request.getRequestDispatcher(Path.PAGE_ERROR_PAGE);
        HttpSession session=request.getSession();
        User user=(User)session.getAttribute("user");
        log.trace("Get user --->"+user);
        Exposition exposition=new Exposition();
        String expositionId=request.getParameter("exposition_id");
        String sort=request.getParameter("sortBy");
        Integer currentPage=Integer.valueOf(request.getParameter("currentPage"));
        request.setAttribute("sortBy",sort);
        request.setAttribute("currentPage",currentPage);
        request.setAttribute("currentView","expositions");
        if(expositionId.isEmpty()){
            String error ="chooseExposition";
            request.setAttribute("errorMessage",error);
            log.error("errorMessage --> " + error);
            requestDispatcher.forward(request,response);
            return;
        }
        exposition.setId(Long.valueOf(expositionId));
        String value=request.getParameter("number");
        if(value.isEmpty()){
            String error ="valueOfTickets";
            request.setAttribute("errorMessage",error);
            log.error("errorMessage --> " + error);
            requestDispatcher.forward(request,response);
            return;
        }

        BigDecimal money=user.getMoney();

        log.trace("user current-->"+user.getMoney());

        HttpSession httpSession=request.getSession();

        BigDecimal cost=new ExpositionDAO().getByID(Long.valueOf(expositionId),new LocaleDAO().
                getIdLocale((String)httpSession.getAttribute("defaultLocale"))).getCost();


        cost=cost.multiply(new BigDecimal(value));

        log.trace("cost-->"+cost);

        if(cost.compareTo(money)>0){
            String error ="priceToHigh";
            request.setAttribute("errorMessage",error);
            log.error("errorMessage --> " + error);
            requestDispatcher.forward(request,response);
            return;
        }

        Integer integer=Integer.valueOf(value);
        log.trace("Initialize exposition with id--->"+exposition.getId());
        log.trace("Initialize number of tickets"+integer);
        new OrderDAO().add(user,exposition,integer);
        user=new UserDAO().getByID(user.getId());
        session.setAttribute("user",user);
        log.trace("user update-->"+user);
        log.debug("Order added");
        response.sendRedirect("controller?command=listOfExpositions&sortBy="+sort+"&currentPage="+currentPage);
        log.debug("Command buyExposition finished");
    }
}
