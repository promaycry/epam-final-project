package com.epam.ua.dnipro.kaliuha.web.command;

import com.epam.ua.dnipro.kaliuha.dao.UserDAO;
import com.epam.ua.dnipro.kaliuha.entity.User;
import com.epam.ua.dnipro.kaliuha.util.Hash;
import com.epam.ua.dnipro.kaliuha.web.Path;
import org.apache.log4j.Logger;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Register command.
 *
 * @author Kaliuha Aleksei
 *
 */
public class RegisterCommand extends Command {

    private static final long serialVersionUID = 7738791314140076456L;

    private static final Logger log = Logger.getLogger(RegisterCommand.class);

    @Override
    public void execute(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        log.debug("Command starts");
        RequestDispatcher requestDispatcher=request.getRequestDispatcher(Path.PAGE_ERROR_PAGE);

        String login=request.getParameter("login");
        String password=request.getParameter("password");
        String passwordConfirm=request.getParameter("password_confirm");
        String email=request.getParameter("email");

        request.setAttribute("currentView","register");

        if(email.isEmpty()||password.isEmpty()||passwordConfirm.isEmpty()||login.isEmpty()){
            String error="email.login.password.empty";
            request.setAttribute("errorMessage", error);
            requestDispatcher.forward(request,response);
            log.error("error--->"+error);
            return;
        }

        if(!passwordConfirm.equals(password)){
            String error="password.confirm";
            request.setAttribute("errorMessage", error);
            requestDispatcher.forward(request,response);
            log.error("error--->"+error);
            return;
        }

        if(!email.matches("[a-z0-9]+@[a-z0-9]+[.](com)||(ua)||(ru)||(net)||(org)")){
            String error="email.invalid";
            request.setAttribute("errorMessage", error);
            requestDispatcher.forward(request,response);
            log.error("error--->"+error);
            return;
        }

        if(new UserDAO().getByLogin(login)||new UserDAO().getByEmail(email)){
            String error="email.login.exist";
            request.setAttribute("errorMessage", error);
            requestDispatcher.forward(request,response);
            log.error("error--->"+error);
            return;
        }

        User user=new User();
        user.setEmail(email);
        user.setLogin(login);
        user.setPassword(Hash.hash(password));
        user.setRole("authorized user");

        new UserDAO().add(user);

        user=new UserDAO().get(user.getLogin(),user.getPassword());

        log.trace("user add--->"+user);

        response.sendRedirect(Path.GO_TO_PAGE_LIST);

        log.debug("Command finished");

    }

}
