package com.epam.ua.dnipro.kaliuha.web.command;

import com.epam.ua.dnipro.kaliuha.dao.UserDAO;
import com.epam.ua.dnipro.kaliuha.entity.User;
import com.epam.ua.dnipro.kaliuha.util.Hash;
import com.epam.ua.dnipro.kaliuha.web.Path;
import org.apache.log4j.Logger;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.jsp.jstl.core.Config;
import java.io.IOException;


/**
 * Update setting
 *
 * @author Kaliuha Aleksei
 *
 */
public class SettingCommand extends Command {

    private static final long serialVersionUID = -2785976618667657267L;

    private static final Logger log = Logger.getLogger(SettingCommand.class);

    @Override
    public void execute(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {

        log.debug("Command start");
        HttpSession httpSession=request.getSession();
        User user= (User) httpSession.getAttribute("user");
        RequestDispatcher requestDispatcher=request.getRequestDispatcher(Path.PAGE_ERROR_PAGE);
        String password="";
        String oldPassword="";
        String email="";
        String oldEmail="";
        if(user!=null){
            log.debug("authorized user setting");
            password= request.getParameter("newPassword");
            oldPassword=  request.getParameter("oldPassword");
            email= request.getParameter("newEmail");
            oldEmail= request.getParameter("oldEmail");
        }

        String locale=request.getParameter("locale");
        String defaultLocale= (String) httpSession.getAttribute("defaultLocale");

        if(locale==null){
            String errorMessage="locale.empty";
            request.setAttribute("errorMessage", errorMessage);
            requestDispatcher.forward(request,response);
            log.error("errorMessage --> " + errorMessage);
            return;
        }

        log.trace("default locale--->"+defaultLocale);
        log.trace("locale to set-->"+locale);

        if(!password.isEmpty()){
            if(!oldPassword.isEmpty()){
                log.trace("user current password--->"+user.getPassword());
             if(Hash.hash(oldPassword).equals(user.getPassword())){
                 log.debug("old password equals to user password");
                 new UserDAO().updateUserPassword(Hash.hash(password),user);
                 user=new UserDAO().getByID(user.getId());
                 log.trace("user changed password --->"+user.getPassword());
                 user=new UserDAO().getByID(user.getId());
                 httpSession.setAttribute("user",user);
                 log.trace("user update-->"+user);
             }
             else {
                 String errorMessage="oldPassword.invalid";
                 request.setAttribute("errorMessage", errorMessage);
                 requestDispatcher.forward(request,response);
                 log.error("error--->"+errorMessage);
                 return;
             }

            }
            else {
                String errorMessage="oldPassword.empty";
                request.setAttribute("errorMessage", errorMessage);
                requestDispatcher.forward(request,response);
                log.error("error--->"+errorMessage);
                return;
            }
        }

        if(!email.isEmpty()){
            log.trace("email to set--->"+email);
            if(!oldEmail.isEmpty()){
                log.trace("old email--->"+oldEmail);
                if(!new UserDAO().getByEmail(email)) {
                    log.debug("this email doesn`t exist in database");
                    if (oldEmail.equals(user.getEmail())) {
                        log.debug("old email equals to user email");
                        new UserDAO().updateUserEmail(email, user);
                        user.setEmail(email);
                        user=new UserDAO().getByID(user.getId());
                        httpSession.setAttribute("user",user);
                        log.trace("user update-->"+user);
                    }
                    else {
                        String errorMessage="oldEmail.invalid";
                        request.setAttribute("errorMessage", errorMessage);
                        requestDispatcher.forward(request,response);
                        log.error("error--->"+errorMessage);
                        return;
                    }
                }
                else{
                    String errorMessage="newEmail.used";
                    request.setAttribute("errorMessage", errorMessage);
                    requestDispatcher.forward(request,response);
                    log.error("error--->"+errorMessage);
                    return;
                }
            }
            else {
                String errorMessage="oldEmail.empty";
                request.setAttribute("errorMessage", errorMessage);
                requestDispatcher.forward(request,response);
                log.error("error--->"+errorMessage);
                return;
            }
        }

        if(!locale.equals(defaultLocale)){
            log.debug("setLocale");
            httpSession.setAttribute("defaultLocale",locale);
            log.trace("defaultLocale set --->"+locale);
            Config.set(httpSession,"javax.servlet.jsp.jstl.fmt.locale",locale);
            log.trace("jsp locale set --->"+locale);
            Cookie[]cookies=request.getCookies();
            boolean find=false;
            log.debug("Searching defaultLocale coockie");
            for(Cookie cookie:cookies){
                if(cookie.getName().equals("defaultLocale")){
                    log.debug("defaultLocale coockie finded");
                    cookie.setValue(locale);
                    response.addCookie(cookie);
                    log.trace("defaultLocale coockie set-->"+locale);
                    find=true;
                }
            }
            if(!find){
                log.debug("defaultLocale coockie not finded, creating new Coockie");
                response.addCookie(new Cookie("defaultLocale",locale));
                log.trace("defaultLocale coockie set-->"+locale);
            }
        }

        String currentPage=request.getParameter("currentView");
        log.trace("currentPage-->"+currentPage);

        if(currentPage.equals("expositions")){
            String curPage=request.getParameter("currentPage");
            String sortBy=request.getParameter("sortBy");
            log.debug("redirect to list of expositions");
            response.sendRedirect("controller?command=listOfExpositions&sortBy="+sortBy+"&currentPage="+curPage);
        }
        else if(currentPage.equals("register")){
            log.debug("redirect to register page");
            response.sendRedirect("controller?command=goToPageRegister");
        }
        else if(currentPage.equals("users")){
            String curPage=request.getParameter("currentPage");
            String orderBy=request.getParameter("orderBy");
            log.debug("redirect to users page");
            response.sendRedirect("controller?command=listOfUsers&orderBy="+orderBy+"&currentPage="+curPage);
        }
        else if(currentPage.equals("orders")){
            String curPage=request.getParameter("currentPage");
            String orderBy=request.getParameter("orderBy");
            log.debug("redirect to orders page");
            response.sendRedirect("controller?command=listOfOrders&orderBy="+orderBy+"&currentPage="+curPage);
        }
        else if(currentPage.equals("addPage")){
            log.debug("redirect to page add");
            response.sendRedirect("controller?command=goToPageAdd");
        }
        else{
            response.sendRedirect(Path.GO_TO_PAGE_LIST);
        }
        log.debug("Command finished");
    }
}
