package com.epam.ua.dnipro.kaliuha.web.command.page;

import com.epam.ua.dnipro.kaliuha.web.Path;
import com.epam.ua.dnipro.kaliuha.web.command.Command;
import org.apache.log4j.Logger;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;


/**
 * Go to listOfUsers.jsp command.
 *
 * @author Kaliuha Aleksei
 *
 */
public class GoToPageUsers extends Command {

    private static final long serialVersionUID = 7738791413038073333L;

    private static final Logger log = Logger.getLogger(GoToPageRegisterCommand.class);
    @Override
    public void execute(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        log.debug("Command start");
        RequestDispatcher requestDispatcher=request.getRequestDispatcher(Path.GO_TO_PAGE_USERS);
        requestDispatcher.forward(request,response);
        log.debug("Command finished");
    }
}
