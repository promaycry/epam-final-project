package com.epam.ua.dnipro.kaliuha.web.command;

import com.epam.ua.dnipro.kaliuha.dao.OrderDAO;
import com.epam.ua.dnipro.kaliuha.dao.UserDAO;
import com.epam.ua.dnipro.kaliuha.entity.Order;
import com.epam.ua.dnipro.kaliuha.entity.User;
import com.epam.ua.dnipro.kaliuha.web.Path;
import org.apache.log4j.Logger;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

/**
 * Delete order command.
 *
 * @author Kaliuha Aleksei
 *
 */
public class DeleteOrderCommand extends Command {

    private static final long serialVersionUID = 8882791314029478343L;

    private static final Logger log = Logger.getLogger(DeleteOrderCommand.class);
    @Override
    public void execute(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        log.debug("Command start");
        Order order=new Order();
        RequestDispatcher requestDispatcher=request.getRequestDispatcher(Path.PAGE_ERROR_PAGE);
        String id=request.getParameter("order");
        String sort=request.getParameter("orderBy");

        Integer currentPage=Integer.valueOf(request.getParameter("currentPage"));

        request.setAttribute("currentView","orders");
        request.setAttribute("orderBy",sort);
        request.setAttribute("currentPage",currentPage);

        if(id.isEmpty()){
            String error="orderNumberToDelete";
            request.setAttribute("errorMessage",error);
            log.error("errorMessage --> " + error);
            requestDispatcher.forward(request,response);
            return;
        }
        HttpSession session=request.getSession();
        User user=(User)session.getAttribute("user");
        order.setId(Long.valueOf(id));
        log.trace("order id set-->"+order.getId());
        new OrderDAO().delete(order);
        log.trace("order deleted id-->"+order.getId());
        user=new UserDAO().getByID(user.getId());
        session.setAttribute("user",user);
        log.trace("user update-->"+user);

        response.sendRedirect("controller?command=listOfOrders&orderBy="+sort+"&currentPage="+currentPage);
    }
}
