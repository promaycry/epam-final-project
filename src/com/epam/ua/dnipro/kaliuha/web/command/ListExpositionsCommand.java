package com.epam.ua.dnipro.kaliuha.web.command;

import com.epam.ua.dnipro.kaliuha.dao.ExpositionDAO;
import com.epam.ua.dnipro.kaliuha.dao.LocaleDAO;
import com.epam.ua.dnipro.kaliuha.dao.UserDAO;
import com.epam.ua.dnipro.kaliuha.entity.Exposition;
import com.epam.ua.dnipro.kaliuha.entity.User;
import com.epam.ua.dnipro.kaliuha.util.ExpositionsQuery;
import com.epam.ua.dnipro.kaliuha.web.Path;
import org.apache.log4j.Logger;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.jsp.jstl.core.Config;
import java.io.IOException;
import java.util.List;


/**
 * Lists of expositions.
 *
 * @author D.Kolesnikov
 *
 */
public class ListExpositionsCommand extends Command{

    private static final Integer ELEMENTS =6;

    private static final long serialVersionUID = 1863978254689586513L;

    private static final Logger log = Logger.getLogger(ListExpositionsCommand.class);

    @Override
    public void execute(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {

        log.debug("Commands starts");

        HttpSession session=request.getSession();

        Cookie[]cookies=request.getCookies();

        boolean find=false;

        String locale=null;

        List<String>locales= LocaleDAO.getLocales();

        String role="user";
        log.trace("user view set--->"+role);

        for(Cookie cookie:cookies){
            log.debug("Seeking coockies");
            if("defaultLocale".equals(cookie.getName())){
                log.debug("Coockies finded");
                locale=cookie.getValue();
                log.trace("locale set--->"+locale);
                Config.set(session,"javax.servlet.jsp.jstl.fmt.locale",locale);
                log.trace("locale of jsp`s set--->"+locale);
                session.setAttribute("defaultLocale",locale);
                log.trace("session atribute defaultLocale set--->"+locale);
                session.setAttribute("locales",locales);
                log.trace("session locales set--->"+locales);
                find=true;
                break;
            }
        }

        if(!find){
            log.debug("Create new coockie");
            locale= "en";
            log.trace("locale set-->"+locale);
            Config.set(session,"javax.servlet.jsp.jstl.fmt.locale",locale);
            session.setAttribute("defaultLocale",locale);
            log.trace("session atribute defaultLocale set--->"+locale);
            session.setAttribute("locales",locales);
            log.trace("session locales set--->"+locales);
            Cookie cookie=new Cookie("defaultLocale",locale);
            response.addCookie(cookie);
            log.trace("coockie set--->"+cookie);
        }

        boolean admin=false;

        if(session.getAttribute("user")!=null){
            log.debug("select admin view");
            User user= (User) session.getAttribute("user");
            user=new UserDAO().getByID(user.getId());
            if(user.getRole().equals("admin")){
                role="admin";
                log.trace("user view set--->"+role);
                admin=true;
                log.debug("admin set--->"+admin);
            }
            user=new UserDAO().getByID(user.getId());
            session.setAttribute("user",user);
            log.trace("user update-->"+user);
        }

        Integer currentPage=Integer.valueOf(request.getParameter("currentPage"));
        log.trace("current page --->"+currentPage);

        String sort=request.getParameter("sortBy");

        log.trace("sorting way --->"+sort);

        log.trace("user view --->"+role);

        log.trace("locale--->"+locale);

        List<Exposition> expositionList=new ExpositionDAO().getAll(locale,
                ExpositionsQuery.get(role,sort),currentPage, ELEMENTS);
        log.trace("expositions get --->"+expositionList);

        request.setAttribute("expositionList",expositionList);
        log.trace("expositionList set --->"+expositionList);
        request.setAttribute("sortBy",sort);
        log.trace("sortBy set --->"+sort);
        request.setAttribute("currentPage",currentPage);
        log.trace("currentPage set --->"+currentPage);

        int count=new ExpositionDAO().getCount(admin);
        log.trace("count of expositions --->"+count);


        int numberOfPages=count/ ELEMENTS;
        log.trace("number of pages--->"+numberOfPages);

        if(count% ELEMENTS !=0){
            numberOfPages++;
            log.trace("number of pages--->"+numberOfPages);
        }

        request.setAttribute("noOfPages",numberOfPages);
        log.trace("number of pages sended--->"+numberOfPages);

        request.setAttribute("currentView","expositions");
        log.trace("set currentView-->expositions");

        RequestDispatcher requestDispatcher=request.getRequestDispatcher(Path.PAGE_EXPOSITION_LIST);
        requestDispatcher.forward(request,response);

        log.debug("Commands finished");
    }

}
