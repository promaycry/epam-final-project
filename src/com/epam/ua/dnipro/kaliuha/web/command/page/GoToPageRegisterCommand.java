package com.epam.ua.dnipro.kaliuha.web.command.page;

import com.epam.ua.dnipro.kaliuha.dao.LocaleDAO;
import com.epam.ua.dnipro.kaliuha.web.Path;
import com.epam.ua.dnipro.kaliuha.web.command.Command;
import org.apache.log4j.Logger;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.jsp.jstl.core.Config;
import java.io.IOException;
import java.util.List;


/**
 * Go to registerPage.jsp command.
 *
 * @author Kaliuha Aleksei
 *
 */
public class GoToPageRegisterCommand extends Command {

    private static final long serialVersionUID = 7738791314038073333L;

    private static final Logger log = Logger.getLogger(GoToPageRegisterCommand.class);

    @Override
    public void execute(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        log.debug("Command start");
        request.setAttribute("currentView","register");
        log.trace("set currentView--->register");
        RequestDispatcher requestDispatcher=request.getRequestDispatcher(Path.PAGE_REGISTER);
        requestDispatcher.forward(request,response);
        log.debug("Command finished");
    }

}
