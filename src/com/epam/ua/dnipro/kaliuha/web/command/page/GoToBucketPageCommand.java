package com.epam.ua.dnipro.kaliuha.web.command.page;

import com.epam.ua.dnipro.kaliuha.web.Path;
import com.epam.ua.dnipro.kaliuha.web.command.Command;
import org.apache.log4j.Logger;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;


/**
 * Go to page bucket.jsp command.
 *
 * @author Kaliuha Aleksei
 *
 */
public class GoToBucketPageCommand extends Command {

    private static final long serialVersionUID = 7738493857038076456L;

    private static final Logger log = Logger.getLogger(GoToPageList.class);

    @Override
    public void execute(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        log.debug("Command starts");
        RequestDispatcher requestDispatcher=request.getRequestDispatcher(Path.PAGE_BUCKET_PAGE);
        requestDispatcher.forward(request,response);
        log.debug("Command finished");
    }

}
