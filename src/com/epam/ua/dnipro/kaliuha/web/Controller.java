package com.epam.ua.dnipro.kaliuha.web;

import com.epam.ua.dnipro.kaliuha.web.command.Command;
import com.epam.ua.dnipro.kaliuha.web.command.CommandContainer;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Main servlet controller.
 *
 * @author Kaliuha Aleksei
 *
 */

@MultipartConfig(maxFileSize = 16177215)
public class Controller extends HttpServlet {

    private static final long serialVersionUID = 2423353715955164816L;

    private static final Logger log = Logger.getLogger(Controller.class);

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
        log.debug("doGet start");
        execute(req,resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        log.debug("doPost start");
        execute(req,resp);
    }


    /**
     * Main method of this controller.
     */
    public void execute(HttpServletRequest request,HttpServletResponse response) throws IOException, ServletException {

        log.debug("Controller starts");
        // extract command name from the request
        String commandName = request.getParameter("command");
        log.trace("Request parameter: command --> " + commandName);
        // obtain command object by its name
        Command command = CommandContainer.get(commandName);
        log.trace("Obtained command --> " + command);

        // execute command and get forward address
        command.execute(request, response);
        log.debug("Controller finished");
    }
}
