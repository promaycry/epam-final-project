package com.epam.ua.dnipro.kaliuha.web.listener;


import com.epam.ua.dnipro.kaliuha.entity.User;
import com.epam.ua.dnipro.kaliuha.web.command.LoginCommand;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;



/**
 * Session listener.
 *
 * @author Kaliuha Aleksei
 *
 */
public class SessionListener implements HttpSessionListener {

    private static final Logger log = Logger.getLogger(SessionListener.class);

    @Override
    public void sessionCreated(HttpSessionEvent se) {
        log.debug("session creating start");
        se.getSession().setMaxInactiveInterval(15*60);
        log.trace("session timeout set to 15 minutes");
        log.debug("session created");
    }

    @Override
    public void sessionDestroyed(HttpSessionEvent se) {
        log.debug("session destroyed start");
        User user=(User)se.getSession().getAttribute("user");
        if(user!=null){
        LoginCommand.delete(user);
        }
        log.debug("session destroyed");
    }

}
