package com.epam.ua.dnipro.kaliuha.web;


/**
 * Path holder (jsp pages, controller commands).
 *
 * @author Kaliuha Aleksei
 *
 */
public final class Path {

    private Path(){

    }

    public static final String PAGE_ERROR_PAGE="view/errorPage.jsp";

    public static final String PAGE_BUCKET_PAGE="view/bucketPage.jsp";

    public static final String MAIN="main.jsp";

    public static final String PAGE_ADD="view/addExposition.jsp";

    public static final String PAGE_REGISTER="view/registerPage.jsp";

    public static final String GO_TO_PAGE_ADD="controller?command=goToPageAdd";

    public static final String PAGE_EXPOSITION_LIST="view/expositionList.jsp";

    public static final String GO_TO_PAGE_BUCKET="controller?command=goToPageBucket&currentPage=";

    public static final String GO_TO_PAGE_LIST="controller?command=goToPageList";

    public static final String PAGE_BUCKET="view/bucket.jsp";

    public static final String LIST_OF_USERS="view/listOfUsers.jsp";

    public static final String GO_TO_PAGE_USERS="view/users.jsp";

}
