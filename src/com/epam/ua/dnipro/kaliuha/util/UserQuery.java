package com.epam.ua.dnipro.kaliuha.util;

import java.util.HashMap;


/**
 * Get sql query for getting users
 *
 * @author Kaliuha Aleksei
 *
 */
public class UserQuery {

    private final static HashMap<String,String> query=new HashMap<>();

    static {

        query.put("loginDec", "Select * from users order by login desc limit ?,?");

        query.put("loginAsc", "Select * from users order by login limit ?,?");

        query.put("emailDec", "Select * from users order by email desc limit ?,?");

        query.put("emailAsc", "Select * from users order by email limit ?,?");

        query.put("roleDec", "Select * from users order by role_id desc limit ?,?");

        query.put("roleAsc", "Select * from users order by role_id limit ?,?");

        query.put("default","Select * from users limit ?,?");

    }
    /**
     * @param sort
     * Select type of sort
     *
     * @return
     * SQL query to get users
     *
     *
     * @author Kaliuha Aleksei
     *
     */
    public static String get( String sort){
        if(!query.containsKey(sort)){
            return query.get("default");
        }
        return query.get(sort);
    }
}
