package com.epam.ua.dnipro.kaliuha.util;

import java.util.HashMap;


/**
 * Get sql query for getting expositions
 *
 * @author Kaliuha Aleksei
 *
 */
public final class ExpositionsQuery {

    private final static HashMap<String,String> comparatorsForUser=new HashMap<>();

    private final static HashMap<String,String> comparatorsForAdmin=new HashMap<>();

    private final static HashMap<String,HashMap<String,String>>roles=new HashMap<>();

    static {

        comparatorsForUser.put("costDec", "Select * from get_expositions_for_user where locale_id=? order by cost desc limit ?,?");

        comparatorsForUser.put("costAsc", "Select * from get_expositions_for_user where locale_id=? order by cost limit ?,?");

        comparatorsForUser.put("dateDec","Select * from get_expositions_for_user where locale_id=? order by date desc limit ?,?");

        comparatorsForUser.put("dateAsc","Select * from get_expositions_for_user where locale_id=? order by date limit ?,?");

        comparatorsForUser.put("topicDec","Select * from get_expositions_for_user where locale_id=? order by topic desc limit ?,?");

        comparatorsForUser.put("topicAsc", "Select * from get_expositions_for_user where locale_id=? order by topic limit ?,?");

        comparatorsForUser.put("ticketsDec", "Select * from get_expositions_for_user where locale_id=? order by (tickets_num-visits) desc limit ?,?");

        comparatorsForUser.put("ticketsAsc","Select * from get_expositions_for_user where locale_id=? order by (tickets_num-visits) limit ?,?");

        comparatorsForUser.put("default","Select * from get_expositions_for_user where locale_id=? limit ?,?");

        comparatorsForAdmin.put("costDec", "Select * from get_expositions where locale_id=? order by cost desc limit ?,?");

        comparatorsForAdmin.put("costAsc", "Select * from get_expositions where locale_id=? order by cost limit ?,?");

        comparatorsForAdmin.put("dateDec","Select * from get_expositions where locale_id=? order by date desc limit ?,?");

        comparatorsForAdmin.put("dateAsc","Select * from get_expositions where locale_id=? order by date limit ?,?");

        comparatorsForAdmin.put("topicDec","Select * from get_expositions where locale_id=? order by topic desc limit ?,?");

        comparatorsForAdmin.put("topicAsc", "Select * from get_expositions where locale_id=? order by topic limit ?,?");

        comparatorsForAdmin.put("ticketsDec", "Select * from get_expositions where locale_id=? order by (tickets_num-visits) desc limit ?,?");

        comparatorsForAdmin.put("ticketsAsc","Select * from get_expositions where locale_id=? order by (tickets_num-visits) limit ?,?");

        comparatorsForAdmin.put("default","Select * from get_expositions where locale_id=? limit ?,?");

        roles.put("user",comparatorsForUser);

        roles.put("admin",comparatorsForAdmin);


    }
    /**
     * @param role
     * Select view
     * @param sort
     * Select type of sort
     *
     * @return
     * SQL query to get expositions
     *
     *
     * @author Kaliuha Aleksei
     *
     */
    public static String get(String role, String sort){
        if(!roles.containsKey(role)){
            return roles.get("user").get("default");
        }
        if(!roles.get(role).containsKey(sort)){
            return roles.get("user").get("default");
        }
        return roles.get(role).get(sort);
    }
}
