<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>


<html>

<title>Error</title>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"
      integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
<link href="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<body>

<div class="card" width="20rem">
    <div class="card-body">
        <h1 class="card-title"><center><fmt:message key="errorOccure"/></center></h1>
            <c:if test="${not empty errorMessage}">
            <h3 class="card-text"><fmt:message key="errorMessage"/>:<fmt:message key="${errorMessage}"/></h3>
            </c:if>
        <form action="controller" method="post">
            <c:if test="${empty currentView}">
    <input type="hidden" name="command" value="goToPageList">
            </c:if>
            <c:if test="${not empty currentView}">
                <c:choose>
                    <c:when test="${currentView eq 'expositions'}">
                        <input type="hidden" name="command" value="listOfExpositions">
                        <input type="hidden" name="currentPage" value="${currentPage}">
                        <input type="hidden" name="sortBy" value="${sortBy}">
                    </c:when>
                    <c:when test="${currentView eq 'orders'}">
                        <input type="hidden" name="command" value="listOfOrders">
                        <input type="hidden" name="currentPage" value="${currentPage}">
                        <input type="hidden" name="orderBy" value="${orderBy}">
                    </c:when>
                    <c:when test="${currentView eq 'register'}">
                        <input type="hidden" name="command" value="goToPageRegister">
                    </c:when>
                    <c:when test="${currentView eq 'users'}">
                        <input type="hidden" name="command" value="listOfOrders">
                        <input type="hidden" name="currentPage" value="${currentPage}">
                        <input type="hidden" name="orderBy" value="${orderBy}">
                    </c:when>
                    <c:when test="${currentView eq 'addPage'}">
                        <input type="hidden" name="command" value="goToPageAdd">
                    </c:when>
                </c:choose>
            </c:if>
    <button type="submit" class="btn btn-primary" ><fmt:message key="back"/> </button>
        </form>
    </div>
</div>
<jsp:include page="../WEB-INF/footer.jsp"/>
</body>
</html>
