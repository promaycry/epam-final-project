<%--
  Created by IntelliJ IDEA.
  User: allha
  Date: 21.10.2020
  Time: 0:45
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<html>
<head>
    <title>Users</title>
</head>
<body>
<jsp:include page="../bar/navigationBar.jsp"/>
<br>
<ul class="nav grey lighten-4 py-4 sortable">
    <li >
        <form action="controller" method="post" id="Refresh">
            <input type="hidden" name="command" value="goToPageUsers">
            <a onclick="document.getElementById('Refresh').submit()"><fmt:message key="refresh.button.jsp"/> </a>
        </form>
    </li>
    <li >
        <a>&#160; <fmt:message key="login.label.jsp"/> &#160;</a>
    </li>
    <li >
        <form action="controller" id="loginUp" method="post">
            <input type="hidden" name="command" value="listOfUsers">
            <input type="hidden" name="currentPage" value="1">
            <input type="hidden" name="orderBy" value="loginAsc">
            <a class="button" onclick="document.getElementById('loginUp').submit()">
                &#8593;
            </a>
        </form>
    </li>
    <li>
        <form action="controller" id="loginDown" method="post">
            <input type="hidden" name="command" value="listOfUsers">
            <input type="hidden" name="currentPage" value="1">
            <input type="hidden" name="orderBy" value="loginDec">
            <a class="button" onclick="document.getElementById('loginDown').submit()">
                &#8595;
            </a>
        </form>
    </li>
    <li >
        <a >&#160; <fmt:message key="email.label.jsp"/> &#160;</a>
    </li>
    <li >
        <form action="controller" id="emailUp" method="post">
            <input type="hidden" name="command" value="listOfUsers">
            <input type="hidden" name="currentPage" value="1">
            <input type="hidden" name="orderBy" value="emailAsc">
            <a class="button" onclick="document.getElementById('emailUp').submit()">
                &#8593;
            </a>
        </form>
    </li>
    <li >
        <form action="controller" id="emailDown" method="post">
            <input type="hidden" name="command" value="listOfUsers">
            <input type="hidden" name="currentPage" value="1">
            <input type="hidden" name="orderBy" value="emailDec">
            <a class="button" onclick="document.getElementById('emailDown').submit()">
                &#8595;
            </a>
        </form>
    </li>
    <li >
        <a >&#160; <fmt:message key="role.label.jsp"/> &#160;</a>
    </li>
    <li >
        <form action="controller" id="roleUp" method="post">
            <input type="hidden" name="command" value="listOfUsers">
            <input type="hidden" name="currentPage" value="1">
            <input type="hidden" name="orderBy" value="roleAsc">
            <a class="button" onclick="document.getElementById('emailUp').submit()">
                &#8593;
            </a>
        </form>
    </li>
    <li >
        <form action="controller" id="roleDown" method="post">
            <input type="hidden" name="command" value="listOfUsers">
            <input type="hidden" name="currentPage" value="1">
            <input type="hidden" name="orderBy" value="roleDec">
            <a class="button" onclick="document.getElementById('roleDown').submit()">
                &#8595;
            </a>
        </form>
    </li>
</ul>
<br>
<div class="card-columns" style="column-count: 3;">
    <c:forEach items="${users}" var="user">
        <div class="card">
            <div class="card-body">
                <h1><fmt:message key="login.label.jsp"/>: ${user.login}</h1>
                <p><fmt:message key="role.label.jsp"/>: ${user.role}</p>
                <p><fmt:message key="email.label"/>: ${user.email}</p>
                <form id="update" method="post" action="controller">
                    <input type="hidden" name="currentPage" value="${currentPage}">
                    <input type="hidden" name="orderBy" value="${orderBy}">
                    <input type="hidden" name="command" value="updateUser">
                    <input type="hidden" name="user" value="${user.id}">
                        <c:if test="${user.role=='authorized user'}">
                            <button class="btn-primary" onclick="document.getElementById('update').submit()"><fmt:message key="setUserAdmin.jsp"/> </button>
                        </c:if>
                        <c:if test="${user.role=='admin'}">
                            <button class="btn-primary" disabled><fmt:message key="already.admin.jsp"/></button>
                        </c:if>
                </form>
            </div>
        </div>
    </c:forEach>
</div>

<ul class="nav grey lighten-4 py-4 sortable">
    <c:if test="${currentPage != 1}">
        <li class="page-item">
            <form action="controller" method="post" id="previous">
                <input type="hidden" name="command" value="listOfUsers">
                <input type="hidden" name="currentPage" value="${currentPage-1}">
                <input type="hidden" name="orderBy" value="${orderBy}">
                <a class="page-link" onclick="document.getElementById('previous').submit()"><fmt:message key="previous"/></a>
            </form>
        </li>
    </c:if>

    <c:forEach begin="1" end="${noOfPages}" var="i">
        <c:choose>
            <c:when test="${currentPage eq i}">
                <li class="page-item active">
                    <form action="controller" method="post" id="page">
                        <input type="hidden" name="command" value="listOfUsers">
                        <input type="hidden" name="currentPage" value="${i}">
                        <input type="hidden" name="orderBy" value="${orderBy}">
                        <a class="page-link" onclick="document.getElementById('page').submit()">${i}</a>
                    </form>
                </li>
            </c:when>
            <c:when test="${currentPage != i}">
                <li class="page-item">
                    <form action="controller" method="post" id="pageOther">
                        <input type="hidden" name="command" value="listOfUsers">
                        <input type="hidden" name="currentPage" value="${i}">
                        <input type="hidden" name="orderBy" value="${orderBy}">
                        <a class="page-link" onclick="document.getElementById('pageOther').submit()">${i}</a>
                    </form>
                </li>
            </c:when>
        </c:choose>
    </c:forEach>

    <c:if test="${currentPage lt noOfPages}">
        <li class="page-item">
            <form action="controller" method="post" id="next">
                <input type="hidden" name="command" value="listOfUsers">
                <input type="hidden" name="currentPage" value="${currentPage+1}">
                <input type="hidden" name="orderBy" value="${orderBy}">
                <a class="page-link" onclick="document.getElementById('next').submit()"><fmt:message key="next"/></a>
            </form>
        </li>
    </c:if>
</ul>
<jsp:include page="../WEB-INF/footer.jsp"/>
</body>
</html>
