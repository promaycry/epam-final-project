<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%--
  Created by IntelliJ IDEA.
  User: allha
  Date: 14.10.2020
  Time: 19:14
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <link rel="stylesheet" href="css/loginPageStyle.css">
</head>
<body>
<div id="id02" class="modal">
    <form class="modal-content animate" id="logOut" action="controller" method="post">
        <input type="hidden" name="command" value="logOut"/>
        <div class="imgcontainer">
            <span onclick="document.getElementById('id02').style.display='none'" class="close" title="Close Modal">&times;</span>
        </div>
        <div class="container">
            <label><b><fmt:message key="logOut.button.jsp"/>?</b></label>
            <button type="submit"><fmt:message key="logOut.button.jsp"/></button>
        </div>
    </form>
</div>
</body>
</html>
