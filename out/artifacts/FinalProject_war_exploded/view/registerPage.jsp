<%--
  Created by IntelliJ IDEA.
  User: allha
  Date: 12.10.2020
  Time: 15:57
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<html>
<head>
    <title>Register</title>
    <link rel="stylesheet" href="https://www.w3schools.com/lib/w3-theme-teal.css">
    <link rel="stylesheet" href="../css/loginPageStyle.css">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link href="//netdna.bootstrapcdn.com/twitter-bootstrap/2.3.2/css/bootstrap-combined.min.css" rel="stylesheet" id="bootstrap-css">
    <script src="//netdna.bootstrapcdn.com/twitter-bootstrap/2.3.2/js/bootstrap.min.js"></script>
    <script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
</head>
<body>
<jsp:include page="/bar/navigationBar.jsp"/>
<div class="container">
<form class="form-horizontal" action='controller' method="POST">
    <input type="hidden" name="command" value="signUp">
    <fieldset>
        <div id="legend">
            <legend class=""><fmt:message key="regestrationForm"/></legend>
        </div>
        <div class="control-group">
            <!-- Username -->
            <label class="control-label"  for="login"><fmt:message key="login.label"/></label>
            <div class="controls">
                <input type="text" id="login" name="login"  class="input-xlarge">
                <p class="help-block"><fmt:message key="underLogin.jsp"/></p>
            </div>
        </div>

        <div class="control-group">
            <!-- E-mail -->
            <label class="control-label" for="email"><fmt:message key="email.label"/></label>
            <div class="controls">
                <input type="text" id="email" name="email"  class="input-xlarge">
                <p class="help-block"><fmt:message key="underEmail.jsp"/> </p>
            </div>
        </div>

        <div class="control-group">
            <!-- Password-->
            <label class="control-label" for="password"><fmt:message key="password.label"/></label>
            <div class="controls">
                <input type="password" id="password" name="password"  class="input-xlarge">
            </div>
        </div>

        <div class="control-group">
            <!-- Password -->
            <label class="control-label"  for="password_confirm"><fmt:message key="passwordConfirm.label"/></label>
            <div class="controls">
                <input type="password" id="password_confirm" name="password_confirm" class="input-xlarge">
                <p class="help-block"><fmt:message key="underPasswordConfirm.jsp"/> </p>
            </div>
        </div>
        <div class="control-group">
            <!-- Button -->
            <div class="controls">
                <button class="btn btn-success"><fmt:message key="register.button.jsp"/></button>
            </div>
        </div>
    </fieldset>
</form>
</div>
<jsp:include page="../WEB-INF/footer.jsp"/>
</body>
</html>
