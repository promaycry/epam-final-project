<%--
  Created by IntelliJ IDEA.
  User: allha
  Date: 11.10.2020
  Time: 18:42
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
    <title>Title</title>
    <link rel="stylesheet" href="https://www.w3schools.com/lib/w3-theme-teal.css">
    <link rel="stylesheet" href="../css/loginPageStyle.css">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
</head>
<body onload="document.forms['main'].submit()">
<form id="main" action="controller" method="post">
    <input type="hidden" name="command" value="listOfOrders">
    <input type="hidden" name="currentPage" value="1">
    <input type="hidden" name="orderBy" value="default">
</form>
</body>
</html>
