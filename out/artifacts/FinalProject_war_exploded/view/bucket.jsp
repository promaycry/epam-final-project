<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
    <link rel="stylesheet" href="https://www.w3schools.com/lib/w3-theme-teal.css">
    <link rel="stylesheet" href="../css/loginPageStyle.css">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
</head>
<body bgcolor="#009688">
<jsp:include page="/bar/navigationBar.jsp"/>
<br>
<ul class="nav grey lighten-4 py-4 sortable">
    <li >
        <form action="controller" method="post" id="Refresh">
            <input type="hidden" name="command" value="goToPageBucket">
            <a onclick="document.getElementById('Refresh').submit()"><fmt:message key="refresh.button.jsp"/> </a>
        </form>
    </li>
    <li >
        <a>&#160; <fmt:message key="topic.label"/> &#160;</a>
    </li>
    <li >
        <form action="controller" id="topicUp" method="post">
            <input type="hidden" name="command" value="listOfOrders">
            <input type="hidden" name="currentPage" value="1">
            <input type="hidden" name="orderBy" value="topicAsc">
            <a class="button" onclick="document.getElementById('topicUp').submit()">
                &#8593;
            </a>
        </form>
    </li>
    <li>
        <form action="controller" id="topicDown" method="post">
            <input type="hidden" name="command" value="listOfOrders">
            <input type="hidden" name="currentPage" value="1">
            <input type="hidden" name="orderBy" value="topicDec">
            <a class="button" onclick="document.getElementById('topicDown').submit()">
                &#8595;
            </a>
        </form>
    </li>
    <li >
        <a >&#160; <fmt:message key="tickets.label"/> &#160;</a>
    </li>
    <li >
        <form action="controller" id="TicketsUp" method="post">
            <input type="hidden" name="command" value="listOfOrders">
            <input type="hidden" name="currentPage" value="1">
            <input type="hidden" name="orderBy" value="ticketsAsc">
            <a class="button" onclick="document.getElementById('TicketsUp').submit()">
                &#8593;
            </a>
        </form>
    </li>
    <li >
        <form action="controller" id="TicketsDown" method="post">
            <input type="hidden" name="command" value="listOfOrders">
            <input type="hidden" name="currentPage" value="1">
            <input type="hidden" name="orderBy" value="ticketsDec">
            <a class="button" onclick="document.getElementById('TicketsDown').submit()">
                &#8595;
            </a>
        </form>
    </li>
    <li >
        <a >&#160; <fmt:message key="ticketPrice.lable"/> &#160;</a>
    </li>
    <li >
        <form action="controller" id="CostDown" method="post">
            <input type="hidden" name="command" value="listOfOrders">
            <input type="hidden" name="currentPage" value="1">
            <input type="hidden" name="orderBy" value="costDec">
            <a class="button" onclick="document.getElementById('CostDown').submit()">
                &#8593;
            </a>
        </form>
    </li>
    <li >
        <form action="controller" id="CostUp" method="post">
            <input type="hidden" name="command" value="listOfOrders">
            <input type="hidden" name="currentPage" value="1">
            <input type="hidden" name="orderBy" value="costAsc">
            <a class="button" onclick="document.getElementById('CostUp').submit()">
                &#8595;
            </a>
        </form>
    </li>
    <li >
        <a > &#160;<fmt:message key="price.label"/> &#160;</a>
    </li>
    <li >
        <form action="controller" id="PriceDown" method="post">
            <input type="hidden" name="command" value="listOfOrders">
            <input type="hidden" name="currentPage" value="1">
            <input type="hidden" name="orderBy" value="priceDec">
            <a class="button" onclick="document.getElementById('PriceDown').submit()">
                &#8593;
            </a>
        </form>
    </li>
    <li >
        <form action="controller" id="PriceUp" method="post">
            <input type="hidden" name="command" value="listOfOrders">
            <input type="hidden" name="currentPage" value="1">
            <input type="hidden" name="orderBy" value="priceAsc">
            <a class="button" onclick="document.getElementById('PriceUp').submit()">
                &#8595;
            </a>
        </form>
    </li>
    <li >
        <a > &#160;<fmt:message key="date.label"/> &#160;</a>
    </li>
    <li >
        <form action="controller" id="DateDown" method="post">
            <input type="hidden" name="command" value="listOfOrders">
            <input type="hidden" name="currentPage" value="1">
            <input type="hidden" name="orderBy" value="dateDec">
            <a class="button" onclick="document.getElementById('DateDown').submit()">
                &#8593;
            </a>
        </form>
    </li>
    <li >
        <form action="controller" id="DateUp" method="post">
            <input type="hidden" name="command" value="listOfOrders">
            <input type="hidden" name="currentPage" value="1">
            <input type="hidden" name="orderBy" value="dateAsc">
            <a class="button" onclick="document.getElementById('DateUp').submit()">
                &#8595;
            </a>
        </form>
    </li>
</ul>
<br>
<div class="card-columns" style="column-count: 3;">
<c:forEach items="${orderList}" var="order">
    <div class="card">
        <div class="card-body">
            <h1 class="card-text">${order.exposition.topic}</h1>
            <img class="card-img" src="data:image/jpg;base64,${order.exposition.image}" width="240" height="300"/>
            <p class="card-text"><fmt:message key="dateOfExpo.label"/>:${order.exposition.date}</p>
            <p class="card-text"><fmt:message key="cost.label"/>:${order.exposition.cost} $</p>
            <p class="card-text"><fmt:message key="fullPrice"/>:${order.cost*order.number} $</p>
            <p class="card-text"><fmt:message key="numberOfTickets"/>:${order.number}</p>
            <form action="controller" method="post" id="returnForm">
                <input type="hidden" name="command" value="deleteOrder">
                <input type="hidden" name="order" value="${order.id}">
                <input type="hidden" name="currentPage" value="${currentPage}">
                <input type="hidden" name="orderBy" value="${orderBy}">
                <c:if test="${order.done==false}">
                <button class="btn-primary" onclick="document.getElementById('returnForm').submit()"><fmt:message key="return.button.jsp"/> </button>
                </c:if>
                <c:if test="${order.done==true}">
                    <button class="btn-primary" disabled><fmt:message key="cantReturn.button.jsp"/></button>
                </c:if>
            </form>
        </div>
    </div>
</c:forEach>
</div>

<ul class="nav grey lighten-4 py-4 sortable">
    <c:if test="${currentPage != 1}">
        <li class="page-item">
            <form action="controller" method="post" id="previous">
                <input type="hidden" name="command" value="listOfOrders">
                <input type="hidden" name="currentPage" value="${currentPage-1}">
                <input type="hidden" name="orderBy" value="${orderBy}">
                <a class="page-link" onclick="document.getElementById('previous').submit()"><fmt:message key="previous"/></a>
            </form>
        </li>
    </c:if>

    <c:forEach begin="1" end="${noOfPages}" var="i">
        <c:choose>
            <c:when test="${currentPage eq i}">
                <li class="page-item active">
                    <form action="controller" method="post" id="page">
                    <input type="hidden" name="command" value="listOfOrders">
                    <input type="hidden" name="currentPage" value="${i}">
                     <input type="hidden" name="orderBy" value="${orderBy}">
                    <a class="page-link" onclick="document.getElementById('page').submit()">${i}</a>
                </form>
                </li>
            </c:when>
            <c:when test="${currentPage != i}">
                <li class="page-item">
                    <form action="controller" method="post" id="pageOther">
                        <input type="hidden" name="command" value="listOfOrders">
                        <input type="hidden" name="currentPage" value="${i}">
                        <input type="hidden" name="orderBy" value="${orderBy}">
                        <a class="page-link" onclick="document.getElementById('pageOther').submit()">${i}</a>
                    </form>
                </li>
            </c:when>
        </c:choose>
    </c:forEach>

    <c:if test="${currentPage lt noOfPages}">
        <li class="page-item">
            <form action="controller" method="post" id="next">
                <input type="hidden" name="command" value="listOfOrders">
                <input type="hidden" name="currentPage" value="${currentPage+1}">
                <input type="hidden" name="orderBy" value="${orderBy}">
                <a class="page-link" onclick="document.getElementById('next').submit()"><fmt:message key="next"/></a>
            </form>
        </li>
    </c:if>
</ul>
<jsp:include page="../WEB-INF/footer.jsp"/>
</body>
</html>
