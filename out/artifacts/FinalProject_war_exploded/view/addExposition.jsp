<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: allha
  Date: 11.10.2020
  Time: 22:01
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>Title</title>
    <link rel="stylesheet" href="https://www.w3schools.com/lib/w3-theme-teal.css">
    <link rel="stylesheet" href="../css/loginPageStyle.css">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
</head>
<body>
<jsp:include page="/bar/navigationBar.jsp"/>
<form action="controller" method="post" accept-charset="utf-8" enctype="multipart/form-data">
    <input type="hidden" name="command" value="addExposition">
    <input type="text" placeholder="<fmt:message key="enNameEnter.jsp"/>" name="name_en" required>
    <input type="text" placeholder="<fmt:message key="ruNameEnter.jsp"/>" name="name_ru" required>
    <input type="text" placeholder="<fmt:message key="uaNameEnter.jsp"/>" name="name_ua" required>
    <input type="number" placeholder="<fmt:message key="priceEnter.jsp"/>" min="0.00" step="0.25" name="price" required>
    <input type="number" placeholder="<fmt:message key="ticketsEnter.jsp"/>" min="1" name="tickets" required>
    <input type="file" placeholder="<fmt:message key="Image.jsp"/> " accept="image/x-png,image/gif,image/jpeg" min="1" name="image" required>
    <label><b><fmt:message key="inputDate.jsp"/></b></label>
    <input type="date"  name="date" required >
    <label><b><fmt:message key="selectHalls.jsp"/> </b></label>
    <select name="halls" multiple required >
        <c:forEach items="${Halls}" var="hall">
            <option>${hall.id}</option>
        </c:forEach>
    </select>
    <input type="submit" value="<fmt:message key="add.button.jsp"/>">
</form>
<jsp:include page="../WEB-INF/footer.jsp"/>
</body>
</html>
