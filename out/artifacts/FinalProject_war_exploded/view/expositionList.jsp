<%--
  Created by IntelliJ IDEA.
  User: allha
  Date: 06.10.2020
  Time: 16:45
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<html>
<head>
    <title>ExpositionList</title>
</head>
<body>
<jsp:include page="../bar/navigationBar.jsp"/>
<br>
<jsp:include page="../bar/sortBar.jsp"/>
<ul class="nav grey lighten-4 py-4 sortable">
    <c:if test="${currentPage != 1}">
        <li class="page-item">
                <a class="page-link" onclick="document.getElementById('previous').submit()"><fmt:message key="previous"/></a>
        </li>
    </c:if>

    <c:forEach begin="1" end="${noOfPages}" var="i">
        <c:choose>
            <c:when test="${currentPage eq i}">
                <li class="page-item active">
                    <a class="page-link" onclick="document.getElementById('page').submit()">${i}</a>
                </li>
            </c:when>
        </c:choose>
    </c:forEach>

    <c:if test="${currentPage lt noOfPages}">
        <li class="page-item">
                <a class="page-link" onclick="document.getElementById('next').submit()"><fmt:message key="next"/></a>
        </li>
    </c:if>
</ul>
</div>
<div class="card-columns" style="column-count: 3;">
    <c:forEach items="${expositionList}" var="exposition">
            <div class="card">
                <div class="card-body">
                <h1 class="card-title"><center>${exposition.topic}</center></h1>
                    <img class="card-img" src="data:image/jpg;base64,${exposition.image}" width="240" height="300"/>
                    <br>
                <p class="card-text"><fmt:message key="dateOfExpo.label"/>:${exposition.date}</p>
                    <p class="card-text"><fmt:message key="cost.label"/>:${exposition.cost} $</p>
                    <p class="card-text"><fmt:message key="tickets.label"/>:${exposition.ticketsNum-exposition.visits}<c:if test="${user.role=='admin'}">
                        <fmt:message key="from"/> ${exposition.ticketsNum}
                        </c:if></p>
                    <p class="card-text"><fmt:message key="hall.label"/></p>
                <c:forEach items="${exposition.halls}" var="hall">
                    <font class="card-text">${hall.id}<font>
                </c:forEach>
                    <c:if test="${not empty user}">
                        <form id="buy form" action="controller" method="post">
                            <c:choose>
                            <c:when test="${user.role=='authorized user'}">
                                <input type="hidden" name="command" value="buyExposition">
                                <input type="hidden" name="exposition_id" value="${exposition.id}">
                                <input type="hidden" name="currentPage" value="${currentPage}">
                                <input type="hidden" name="sortBy" value="${sortBy}">
                                <input type="hidden" name="currentView" value="${currentView}">
                                <input type="hidden" name="costOfExposition" value="${exposition.cost}" id="price"/>
                                <input type="number" name="number" min="1" max="${exposition.ticketsNum-exposition.visits}" id="number" oninput="calc()"  required>
                                <p class="card-text"><fmt:message key="fullPrice"/>:<span id="total">0</span></p>
                                <script>
                                    function calc()
                                    {
                                        var price = document.getElementById("price").value;
                                        var noTickets = parseInt(document.getElementById("number").value);
                                        var total = parseFloat(price * noTickets);
                                        if(!isNaN(total))
                                            document.getElementById("total").innerHTML = Number(total);
                                    }
                                </script>
                                <button type="submit" class="btn btn-primary"><fmt:message key="buy.button.jsp" /></button>
                            </c:when>
                            <c:when test="${user.role=='admin'}">
                                <input type="hidden" name="command" value="deleteExposition">
                                <input type="hidden" name="exposition_id" value="${exposition.id}">
                                <input type="hidden" name="currentPage" value="${currentPage}">
                                <input type="hidden" name="sortBy" value="${sortBy}">
                                <input type="hidden" name="currentView" value="${currentView}">
                                <button type="submit" class="btn btn-primary"><fmt:message key="delete.button.jsp"/></button>
                            </c:when>
                            </c:choose>
                        </form>
                    </c:if>
            </div>
    </div>
    </c:forEach>
</div>
<ul class="nav grey lighten-4 py-4 sortable">
            <c:if test="${currentPage != 1}">
                <li class="page-item">
                    <form action="controller" method="post" id="previous">
                        <input type="hidden" name="command" value="listOfExpositions">
                        <input type="hidden" name="currentPage" value="${currentPage-1}">
                        <input type="hidden" name="sortBy" value="${sortBy}">
                    <a class="page-link" onclick="document.getElementById('previous').submit()"><fmt:message key="previous"/></a>
                    </form>
                </li>
            </c:if>

            <c:forEach begin="1" end="${noOfPages}" var="i">
                <c:choose>
                    <c:when test="${currentPage eq i}">
                        <li class="page-item active"><form action="controller" method="post" id="page">
                            <input type="hidden" name="command" value="listOfExpositions">
                            <input type="hidden" name="currentPage" value="${i}">
                            <input type="hidden" name="sortBy" value="${sortBy}">
                            <a class="page-link" onclick="document.getElementById('page').submit()">${i}</a>
                        </form>
                        </li>
                    </c:when>
                </c:choose>
            </c:forEach>

            <c:if test="${currentPage lt noOfPages}">
                <li class="page-item">
                    <form action="controller" method="post" id="next">
                    <input type="hidden" name="command" value="listOfExpositions">
                    <input type="hidden" name="currentPage" value="${currentPage+1}">
                    <input type="hidden" name="sortBy" value="${sortBy}">
                    <a class="page-link" onclick="document.getElementById('next').submit()"><fmt:message key="next"/></a>
                </form>
                </li>
            </c:if>
        </ul>
</div>
<jsp:include page="../WEB-INF/footer.jsp"/>
</body>

</html>
