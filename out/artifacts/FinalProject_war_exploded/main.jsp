<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
</head>
<body onload="document.forms['main'].submit()">
<form id="main" action="controller" method="post">
  <input type="hidden" name="command" value="listOfExpositions">
  <input type="hidden" name="currentPage" value="1">
  <input type="hidden" name="sortBy" value="default">
</form>
<jsp:include page="view/expositionList.jsp"/>
</body>
</html>

